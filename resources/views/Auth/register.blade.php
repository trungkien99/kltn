@extends('Layouts.backend')
@section('login')
<title>Đăng kí thành viên</title>
@endsection
@section('content')
{{--@if ( Session::has('success') )--}}
	{{--<div class="alert alert-success alert-dismissible" role="alert">--}}
		{{--<strong>{{ Session::get('success') }}</strong>--}}
		{{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
			{{--<span aria-hidden="true">&times;</span>--}}
			{{--<span class="sr-only">Close</span>--}}
		{{--</button>--}}
	{{--</div>--}}
{{--@endif--}}
{{--@if ( Session::has('error') )--}}
	{{--<div class="alert alert-danger alert-dismissible" role="alert">--}}
		{{--<strong>{{ Session::get('error') }}</strong>--}}
		{{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
			{{--<span aria-hidden="true">&times;</span>--}}
			{{--<span class="sr-only">Close</span>--}}
		{{--</button>--}}
	{{--</div>--}}
{{--@endif--}}
{{--@if ($errors->any())--}}
	{{--<div class="alert alert-danger alert-dismissible" role="alert">--}}
		{{--<ul>--}}
            {{--@foreach ($errors->all() as $error)--}}
                {{--<li>{{ $error }}</li>--}}
            {{--@endforeach--}}
        {{--</ul>--}}
		{{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
			{{--<span aria-hidden="true">&times;</span>--}}
			{{--<span class="sr-only">Close</span>--}}
		{{--</button>--}}
	{{--</div>--}}
{{--@endif--}}
<div class="container" style="margin-top: 10%">
	<div class="row">
		<div class="col-sm-6 col-md-4 col-md-offset-4">
			<div class="panel panel-default">
                @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('success') }}
                    </div>
                @endif
				<div class="panel-heading">
					<h4 class="panel-title">ĐĂNG KÝ THÀNH VIÊN</h4>
				</div>
				<div class="panel-body">
					<form role="form" method="POST" action="{{ route('postRegister') }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group has-warning">
			                <label>Họ tên</label>
			                <input type="text" value="{{ old('name') }}" class="form-control" id="inputWarning" placeholder="Họ tên" name="name">
                            @if ($errors->has('name'))
                                <span style="color: red">{{ $errors->first('name') }}</span>
                            @endif
                            <br>
			                <label>Email</label>
			                <input type="text" value="{{ old('email') }}" class="form-control" id="inputWarning" placeholder="Email" name="email">
                            @if ($errors->has('email'))
                                <span style="color: red">{{ $errors->first('email') }}</span>
                            @endif
                            <br>
			                <label>Mật khẩu</label>
			                <input type="password" class="form-control" id="inputWarning" placeholder="Mật khẩu" name="password">
                            @if ($errors->has('password'))
                                <span style="color: red">{{ $errors->first('password') }}</span>
                            @endif
                            <br>
			                <label>Nhập lại mật khẩu</label>
			                <input class="form-control" placeholder="Xác nhận mật khẩu" name="password_confirmation" type="password">
                            @if ($errors->has('password'))
                                <span style="color: red">{{ $errors->first('password') }}</span>
                            @endif
                            <br>
			                <label>Số điện thoại</label>
			                <input type="text" value="{{ old('phone') }}" class="form-control" id="inputWarning" placeholder="Số điện thoại" name="phone">
                            @if ($errors->has('phone'))
                                <span style="color: red">{{ $errors->first('phone') }}</span>
                            @endif
                            <br>
			                <label>Địa chỉ</label>
			                <input type="text" value="{{ old('address') }}" name="address" placeholder="Địa chỉ" class="form-control">
                            @if ($errors->has('address'))
                                <span style="color: red">{{ $errors->first('address') }}</span>
                            @endif
                            <br>
			                <label>Ảnh đại diện</label>
			                <input type="file" class="form-control" id="inputWarning" placeholder="Ảnh đại diện" name="avatar">
                            @if ($errors->has('avatar'))
                                <span style="color: red">{{ $errors->first('avatar') }}</span>
                            @endif
			            </div>
						<div class="form-group">
							<button type="submit" class="btn btn-lg btn-primary btn-block">Đăng ký</button>
						</div>
						<center><a href="{{ route('login') }}">Quay về đăng nhập</a></center>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
body{
	background: #17568C;
}
.panel{
	border-radius: 5px;
}
.panel-heading {
    padding: 10px 15px;
}
.panel-title{
	text-align: center;
	font-size: 15px;
	font-weight: bold;
	color: #17568C;
}
.panel-footer {
	padding: 1px 15px;
	color: #A0A0A0;
}
.profile-img {
	width: 120px;
	height: 120px;
	margin: 0 auto 10px;
	display: block;
	-moz-border-radius: 50%;
	-webkit-border-radius: 50%;
	border-radius: 50%;
}
</style>
@endsection
