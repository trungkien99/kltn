@extends('Layouts.frontend')

@section('content')
<div class="content-homepage">
    <!-- slide-homepage-1-->
    @include('Layouts.partions.slide')
    <!--end slide-homepage-1-->
    <!--item homepage-->
    <div class="container" style="padding-top: 30px">
        <div class="row item-homepage">
            <div class="col-lg-4 col-md-4 col-sm-12 col-12 content-item-homepage">
                <div class="wpb_wrapper">
                    <div class="banner-section">
                        <div class="inner-banner-section">
                            <h2>Bền bỉ</h2>
                            <img src="{{ asset('imager/home/banner-1.png')}}" alt="" class="banner-item-homepage">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-12 content-item-homepage">
                <div class="wpb_wrapper">
                    <div class="banner-section">
                        <div class="inner-banner-section">
                            <h2>Mạnh mẽ</h2>
                            <img src="{{ asset('imager/home/banner-2.jpg')}}" alt="" class="banner-item-homepage">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-12 content-item-homepage">
                <div class="wpb_wrapper">
                    <div class="banner-section">
                        <div class="inner-banner-section">
                            <h2>Thời trang</h2>
                            <img src="{{ asset('imager/home/banner-3.png') }}" alt="" class="banner-item-homepage">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end item homepage-->
    <!--Product out-->
    <div class="product-out-homepage2">
        <div class="container">
            <div class="product-out">
{{--                sản phẩm mới--}}
                <div class="container">
                    <div class="row content-follow-insta">
                        <div class="title-follow-insta">
                            <h2>Sản phẩm mới</h2>
                        </div>
                        <div class="owl-carousel owl-product owl-theme">
                            @foreach($products as $product)
                                <div class=" col-xl-12 col-lg-12 col-md-12 col-sm-6 col-12 product">
                                    <div class="card">
                                        <div class="card-img-top">
                                            @if($product->qty_nhap != 0)
                                                <a href="{{ route('frontend.product.show',['id'=>$product->id]) }}"
                                                   class="wp-post-image" style="background-color: #dfdfdf">
                                                    <img style="max-height: 255px;border: 1px solid #bdb7b7"
                                                         class="image-cover"
                                                         src="{{ asset('Uploads/products/'.$product->image)}}"
                                                         alt="product">
                                                </a>
                                                @if($product->status == 1)
                                                    <p class="onnew">New</p>
                                                @elseif($product->status == 0)
                                                    <p class="onsale">Sale</p>
                                                @else

                                                @endif
                                                <div class="icon-product">
                                                    <a href="{{ route('frontend.cart.insertSecound',['id'=>$product->id])}}"><span
                                                            class="lnr lnr-lock btn prbtn"
                                                            style="background-color: #ffa6a8; color: white; border-radius: 50%; bottom: 5px;"></span></a>
                                                    <a href="{{ route('frontend.product.show',['id'=>$product->id]) }}"><span
                                                            class="lnr lnr-magnifier btn prbtn"
                                                            style="background-color: #ffa6a8; color: white; border-radius: 50%;"></span>
                                                    </a>
                                                </div>
                                            @else
                                                <a href="#" class="wp-post-image"
                                                   style="background-color: #dfdfdf; opacity: 0.5;">
                                                    <img style="max-height: 255px" class="image-cover"
                                                         style="opacity: 0.5 !important;"
                                                         src="{{ asset('Uploads/products/'.$product->image)}}"
                                                         alt="product">
                                                </a>
                                                <p class="onsale">Hết hàng</p>
                                            @endif
                                        </div>
                                        <div class="card-body">
                                            <p class="card-title"><a href="{{ route('frontend.product.show',['id'=>$product->id]) }}">{{$product->name}}</a></p>
                                            <p class="woocommerce-loop-product__title"><a href="{{ route('frontend.product.show',['id'=>$product->id]) }}">{{ trim(substr( $product->product_name, 0, 35 )) }} ...</a></p>
                                            @if($product->status == 0)
                                                <span style="text-decoration: line-through;">{{ number_format(ceil((-$product->price * 100) / ($product->sale - 100)), 0, '', ',') }}
                                                    VND</span>
                                            @endif
                                            <span class="price">
                                                <ins>
                                                    <span class="woocommerce-Price-amount amount">
                                                            {{number_format($product->price, 0, '', ',')}} <span class="woocommerce-Price-currencySymbol">VND</span>
                                                    </span>
                                                </ins>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>


                    </div>
                </div>
{{--                sản phẩm giảm giá--}}
                <div class="title">
                    <h2 class="text-center">Sản phẩm giảm giá</h2>
                </div>
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row product">
                                <div class="owl-carousel owl-product-sale owl-theme col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                @foreach($product_sale as $product)
                                    <div class="">
                                        <div class="card">
                                            <div class="card-img-top">
                                                @if($product->qty_nhap != 0)
                                                    <a href="{{ route('frontend.product.show',['id'=>$product->id]) }}"
                                                       class="wp-post-image" style="background-color: #dfdfdf">
                                                        <img style="max-height: 255px;border: 1px solid #bdb7b7"
                                                             class="image-cover"
                                                             src="{{ asset('Uploads/products/'.$product->image)}}"
                                                             alt="product">
                                                    </a>
                                                    @if($product->status == 1)
                                                        <p class="onnew">New</p>
                                                    @elseif($product->status == 0)
                                                        <p class="onsale">Sale</p>
                                                    @else

                                                    @endif
                                                    <div class="icon-product">
                                                        <a href="{{ route('frontend.cart.insertSecound',['id'=>$product->id])}}"><span
                                                                class="lnr lnr-lock btn prbtn"
                                                                style="background-color: #ffa6a8; color: white; border-radius: 50%; bottom: 5px;"></span></a>
                                                        <a href="{{ route('frontend.product.show',['id'=>$product->id]) }}"><span
                                                                class="lnr lnr-magnifier btn prbtn"
                                                                style="background-color: #ffa6a8; color: white; border-radius: 50%;"></span>
                                                        </a>
                                                    </div>
                                                @else
                                                    <a href="#" class="wp-post-image"
                                                       style="background-color: #dfdfdf; opacity: 0.5;">
                                                        <img style="max-height: 255px" class="image-cover"
                                                             style="opacity: 0.5 !important;"
                                                             src="{{ asset('Uploads/products/'.$product->image)}}"
                                                             alt="product">
                                                    </a>
                                                    <p class="onsale">Hết hàng</p>
                                                @endif
                                            </div>
                                            <div class="card-body">
                                                <p class="card-title"><a href="{{ route('frontend.product.show',['id'=>$product->id]) }}">{{$product->name}}</a></p>
                                                <p class="woocommerce-loop-product__title"><a href="{{ route('frontend.product.show',['id'=>$product->id]) }}">{{ trim(substr( $product->product_name, 0, 35 )) }} ...</a></p>
                                                @if($product->status == 0)
                                                    <span style="text-decoration: line-through;">{{ number_format(ceil((-$product->price * 100) / ($product->sale - 100)), 0, '', ',') }}
                                                    VND</span>
                                                @endif
                                                <span class="price">
                                                <ins>
                                                    <span class="woocommerce-Price-amount amount">
                                                            {{number_format($product->price, 0, '', ',')}} <span class="woocommerce-Price-currencySymbol">VND</span>
                                                    </span>
                                                </ins>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 sale-product">
                                    <a href="#"><img src="{{ asset('imager/home/Sale.png')}}" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p style="color: black"><a href="{{ route('frontend.category.index') }}">Xem thêm</a></p>
                </div>
            </div>
        </div>
    </div>
    <!--end Product out-->
    <!--introduce homepage-->
    <div class="container">
        <div class="row introduce-homepage">
            <div class="introduce-left">
                <div class="row introduce-homepage-left">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 img-introduce">
                        <img src="{{ asset('imager/home/banner-1.png')}}" alt="" height="600px">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 introduce-homepage-content">
                        <h2>0.1</h2>
                        <h5>Bền Bỉ</h5>
                        <p>Ở đế giữa, đệm DNA AMP mang lại độ bật tuyệt vời. Nó cũng có độ bền cao nhờ lớp vỏ TPU độc đáo bao bọc bên ngoài. Bạn sẽ thấy miếng đệm ở Brooks Levitate 5 giữ được độ nảy lâu hơn nhiều so với một đôi giày chạy bộ tiêu chuẩn.
                        </p>
                    </div>
                </div>
            </div>

            <div class="introduce-right">
                <div class="row introduce-homepage-right">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-0 col-0 introduce-homepage-content sub-intro1">
                        <h2>0.2</h2>
                        <h5>Mạnh Mẽ</h5>
                        <p> Với các kiểu dáng thiết kế hiện dại, trẻ trung, các đôi giày dễ dàng tạo ra sức hút và phong cách mạnh mẽ cho người mang.
                        </p>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 img-introduce">
                        <img src="{{ asset('imager/home/banner-2.jpg')}}" alt="" height="600px">
                    </div>

                </div>
            </div>

            <div class="introduce-left">
                <div class="row introduce-homepage-left">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 img-introduce">
                        <img src="{{ asset('imager/home/banner-3.png')}}" alt="" height="600px">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 introduce-homepage-content">
                        <h2>0.3</h2>
                        <h5>Thời trang</h5>
                        <p>Hiện nay có rất nhiều mẫu giày mới xuất hiện trên thị trường, nhưng các mẫu của shop vẫn đa dạng phong phú với các mẫu thời trang phong cách mang lại cái nhìn mới mẻ cho người tiêu dùng.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row content-follow-insta">
            <div class="title-follow-insta">
                <h2>Theo dõi Instagram</h2>
                <p>@Uray.cosmetic_beauty</p>
            </div>
            <div class="owl-carousel owl-theme">
                <div class="item"><a href="#"><img src="{{ asset('imager/home/anh-giay-the-thao-1.jpg')}}" alt=""></a></div>
                <div class="item"><a href="#"><img src="{{ asset('imager/home/anh-giay-the-thao-2.jpg')}}" alt=""></a></div>
                <div class="item"><a href="#"><img src="{{ asset('imager/home/anh-giay-the-thao-3.jpg')}}" alt=""></a></div>
                <div class="item"><a href="#"><img src="{{ asset('imager/home/anh-giay-the-thao-4.jpg')}}" alt=""></a></div>
                <div class="item"><a href="#"><img src="{{ asset('imager/home/anh-giay-the-thao-5.jpg')}}" alt=""></a></div>
                <div class="item"><a href="#"><img src="{{ asset('imager/home/anh-giay-the-thao-6.jpg')}}" alt=""></a></div>
                <div class="item"><a href="#"><img src="{{ asset('imager/home/anh-giay-the-thao-1.jpg')}}" alt=""></a></div>
                <div class="item"><a href="#"><img src="{{ asset('imager/home/anh-giay-the-thao-2.jpg')}}" alt=""></a></div>
                <div class="item"><a href="#"><img src="{{ asset('imager/home/anh-giay-the-thao-3.jpg')}}" alt=""></a></div>
            </div>


        </div>
    </div>
    <!--end Follow Instagram-->
</div>
@endsection
