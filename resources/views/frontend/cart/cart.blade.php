@extends('Layouts.frontend')
@section('content')
    <div class="title-page"
         style="background-image: url('{{ asset('imager/shop/Shop_3Columns-title.jpg')}}');background-position: center center;background-size: cover;">
        <div class="container">
            <div class="row">
                <div class=" col-md-6 inner-title-page">

                    <h1> Giỏ hàng </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="content-cart">
            <div style="margin-bottom: 10px">
                <a href="{{ route('frontend.cart.list') }}">
                    <button type="button" class="btn btn-primary"> Giỏ hàng</button>
                </a>
                <a href="{{ route('frontend.cart.list.order') }}">
                    <button type="button" class="btn btn-primary"> Đơn hàng</button>
                </a>
            </div>
            <form method="get" action="{{ route('order.index')}}">
                {{ csrf_field() }}
                <table class="table cart-desktop">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col" style="text-align: left">Sản phẩm</th>
                        <th scope="col">Đơn giá</th>
                        <th scope="col">Số lượng</th>
                        <th scope="col">Size</th>
                        <th scope="col">Tổng tiền</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($products))
                        @foreach($products as $product)
                            <tr>
                                <td><a href="{{ route('frontend.cart.delete',['id'=>$product->id]) }}">X</a></td>
                                <td>
                                    <div class="row">
                                        <img width="100px" height="100px"
                                             src="{{asset('Uploads/products/'.$product->image)}}"
                                             alt="">
                                        <p>{{$product->product_name}}</p>
                                    </div>
                                </td>
                                <td>{{ number_format($product->price, 0, '', ',') }} VND</td>
                                <td>
                                    <div class="btn-group">
                                        <input style="width: 50px; text-align: center; border: none;" type="number"
                                               name="qty[{{$product->id}}]" value="{{$product->qty}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <input style="width: 50px; text-align: center; border: none;" type="text"
                                               name="{{$product->size}}" value="{{$product->size}}">
                                    </div>
                                </td>
                                <td>{{ number_format($product->price * $product->qty, 0, '', ',') }} VND</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                Không có dữ liệu
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="rocart-total">
                    <h2>Tổng đơn hàng</h2>
                    @if(!empty($products))
                        <ul class="list-inline">
                            <li class="list-inline-item"><p>Tổng tiền</p></li>
                            <input type="hidden" value="{{ number_format($products->total, 0, '', ',') }}" name="productsTotal">
                            <li class="list-inline-item"><p>{{ number_format($products->total, 0, '', ',') }} VND </p></li>
                        </ul>
                    @else
                        <ul class="list-inline">
                            <li class="list-inline-item"><p>Tổng tiền</p></li>
                            <input type="hidden" value="0" name="productsTotal">
                            <li class="list-inline-item"><p>0 VND </p></li>
                        </ul>
                    @endif
                </div>
                <div class="card-button">
                    @if(Auth::check())
                        @if(count($products) > 0)
                            <a href="{{ route('order.index') }}">
                                <input type="button" class="btn update float-right" name="submit" value="MUA HÀNG">
                            </a>
                        @else
                            <input disabled type="submit" class="btn update float-right" name="submit" value="MUA HÀNG">
                        @endif
                    @else
                        <a href="{{ route('login') }}">
                            <input type="button" class="btn update float-right" value="Đăng nhập để mua hàng">
                        </a>
                    @endif
                </div>
            </form>
        </div>
    </div>
@endsection
