@extends('Layouts.frontend')
@section('content')
    <div class="title-page"
         style="background-image: url('{{ asset('imager/shop/Shop_3Columns-title.jpg')}}');background-position: center center;background-size: cover;">
        <div class="container">
            <div class="row">
                <div class=" col-md-6 inner-title-page">
                    <h1> Đơn hàng </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="content-cart">
            {{--@if ( Session::has('errors'))--}}
            {{--<div class="alert alert-success" role="alert">--}}
            {{--{{ $errors->first('message') }} alo--}}
            {{--</div>--}}
            {{--@endif--}}
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>
            @endif

            <div style="margin-bottom: 10px">
                <a href="{{ route('frontend.cart.list') }}">
                    <button type="button" class="btn btn-primary"> Giỏ hàng</button>
                </a>
                <a href="{{ route('frontend.cart.list.order') }}">
                    <button type="button" class="btn btn-primary"> Đơn hàng</button>
                </a>
            </div>
            <form method="post" action="{{ route('frontend.cart.update')}}">
                {{ csrf_field() }}
                <table class="table cart-desktop">
                    <thead>
                    <tr>
                        <th scope="col" style="text-align: left">Sản phẩm</th>
                        <th scope="col">Đơn giá</th>
                        <th scope="col">Số lượng</th>
                        <th scope="col">Size</th>
                        <th scope="col">Tổng tiền</th>
                        <th scope="col">Trạng thái đơn hàng</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($products) > 0)
                        @foreach($products as $product)
                            <tr>
                                {{--<td><a href="{{ route('frontend.cart.delete',['id'=>$product['id']]) }}">X</a></td>--}}
                                <td>
                                    <div class="row">
                                        <img width="100px" height="100px"
                                             src="{{asset('Uploads/products/'.$product['img_prod'])}}"
                                             alt="">
                                        <p>{{$product['product_name']}}</p>
                                    </div>
                                </td>
                                <td>{{ number_format($product['product_price'], 0, '', ',') }} VND</td>
                                <td>
                                    <div class="btn-group">
                                        <input style="width: 50px; text-align: center; border: none;" type="number"
                                               name="qty[{{$product['id']}}]" value="{{$product['qty']}}">
                                    </div>
                                </td>
                                <td>{{$product['size']}}</td>
                                <td>{{ number_format($product['product_price'] * $product['qty'], 0, '', ',') }} VND</td>
                                <td>
                                    @if($product['status_order'] === 0)
                                        <span style="color: red">Đang chờ xử lí</span>
                                    @endif
                                    @if($product['status_order'] === 1)
                                        <span style="color: blue">Đang giao hàng</span>
                                    @endif
                                    @if($product['status_order'] === 2)
                                        <span style="color: green">Đã giao</span>
                                    @endif
                                        @if($product['status_order'] === 3)
                                            <span style="color: green">Đã Hủy</span>
                                        @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                Không có dữ liệu
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                {{--<div class="rocart-total">--}}
                {{--<h2>Tổng đơn hàng</h2>--}}
                {{--<ul class="list-inline">--}}
                {{--<li class="list-inline-item"><p>Tổng tiền</p></li>--}}
                {{--<input type="hidden" value="{{$products->total}}" name="productsTotal">--}}
                {{--<li class="list-inline-item"><p>{{ $products->total }} VND </p></li>--}}
                {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="card-button">--}}
                {{--<input type="submit" class="btn update float-right" name="submit" value="MUA HÀNG">--}}
                {{--</div>--}}
            </form>
        </div>
    </div>
@endsection
