@extends('Layouts.frontend')
@section('content')
    <!--title page-->
    <div class="title-page"
         style="background-image: url('{{ asset('imager/shop/Shop_3Columns-title.jpg') }}');background-position: center center;background-size: cover;">
        <div class="container">
            <div class="row">
                <div class=" col-md-6 inner-title-page">
                    <h1>Thông tin đặt hàng</h1>
                </div>
            </div>
        </div>
    </div>
    <!--end title page-->
    <div class="container">
        <div class="content-checkout">
            <div class="billing-detail">
                <form method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6 " >
                            <h2>Thông tin đặt hàng</h2>
                            <p>Họ tên<span>*</span></p>
                            <input type="text" value="{{auth()->user()->name}}" placeholder="Họ Tên" name="user_name">
                            @if ($errors->has('user_name'))
                                <span style="color: red">{{ $errors->first('user_name') }}</span>
                            @endif
                            <br>
                            <p>Địa chỉ<span>*</span></p>
                            <input type="text" placeholder="Địa chỉ" value="{{auth()->user()->address}}" name="address">
                            @if ($errors->has('address'))
                                <span style="color: red">{{ $errors->first('address') }}</span>
                            @endif
                            <br>
                            <div class="row">
                                <div class="col-md-6"><p>Điện thoại<span>*</span></p>
                                    <input type="number" placeholder="Số điện thoại" value="{{auth()->user()->phone }}" maxlength="10" name="phone">
                                    @if ($errors->has('phone'))
                                        <span style="color: red">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-6"><p>Email<span>*</span></p>
                                    <input type="text" placeholder="Email" value="{{auth()->user()->email }}" name="email">
                                    @if ($errors->has('email'))
                                        <span style="color: red">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <h3>Thông tin thêm</h3>
                            <p>Ghi chú</p>
                            <textarea placeholder="Ghi chú về đơn hàng của bạn" name="note"></textarea>
                            @if ($errors->has('note'))
                                <span style="color: red">{{ $errors->first('note') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 ">
                            <div class="item-checkout">
                                <h2>Đơn hàng của bạn</h2>
                                <div class="item-checkout-detail" style="border-bottom: 1px solid #e6e6e6;">
                                    @foreach(session('carts') as $cart)
                                        <div class="card">
                                            <div class="row no-gutters">
                                                <div class="col-md-2">
                                                    <img src="{{ asset('Uploads/products/'.$cart->image)}}" class="card-img" alt="...">
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="card-body" style="margin-left: 50px">
                                                        <p class="card-text">{{$cart->product_name}}</p>
                                                        <p class="card-text"><b>Số lượng : </b>x {{$cart->qty}}</p>
                                                        <p class="card-text"><b>Size : </b> {{$cart->size}}</p>
                                                        <span class="price">
                                                            <b>Giá tiền : </b>
                                                            <ins>
                                                                <span class="woocommerce-Price-amount amount">
                                                                   {{number_format($cart->price * $cart->qty, 0, '', ',')}} <span
                                                                        class="woocommerce-Price-currencySymbol">VND</span>
                                                                </span>
                                                            </ins>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="sub-payment">
                                    <h5>Tổng tiền &nbsp;&nbsp;&nbsp;<span>{{number_format(session('carts')->total, 0, '', ',')}}
                                            VND</span></h5>
                                </div>
                                <div class="payment">
                                    <div class="content-type-payment">
                                        <p>
                                            Vui lòng kiểm tra lại số điện thoại, địa chỉ nhận hàng để giúp quá trình vận chuyển hàng
                                            thêm nhanh chóng!
                                        </p>
                                        <input type="radio" class="btn-check checkedsize" name="payment" id="cod" autocomplete="off" value="cod">
                                        <label class="btn btn-secondary all-check" style="background: #007bff" for="cod" id="ship-code">Thanh toán khi nhận hàng</label>
                                        <input type="radio" class="btn-check checkedsize" name="payment" id="nl" autocomplete="off" value="nl">
                                        <label class="btn btn-secondary all-check" style="background: #007bff" for="nl" id="ship-code">Thanh toán quan Ngân Lượng</label>
                                    </div>

                                </div>
                                <div style="width: 100%;text-align: center;margin-top: 40px;">
                                    <button class="btn">ĐẶT HÀNG</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
