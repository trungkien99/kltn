@extends('Layouts.frontend')
@section('content')
<div class="content-about">
    <!--title page-->
    <div class="title-page"
         style="background-image: url('{{ asset('imager/title-page1.jpg')}}');background-position: center center;background-size: cover;">
        <div class="container">
            <div class="row">
                <div class=" col-md-6 inner-title-page">
                    <h1>Giới thiệu</h1>
                </div>
            </div>
        </div>
    </div>
    <!--end title page-->
    <!--content about-->
    <div class="about">
        <div class="container">
            <div class="row">
                <p>Giày thể thao sneaker là một loại giày quá quen thuộc với cộng đồng và văn hóa tại Việt Nam. Chúng ta sử dụng giày sneaker thường ngày vào các mục đích khác nhau như: chạy bộ, tập gym, thể dục thể thao, hay đi dạo, đi chơi, đi làm... Hiện nay, giày sneaker đang là xu hướng trending trong cộng đồng và tiếp tục ngày càng tăng độ hot cũng như ưa chuộng bởi tính tiện lợi và dễ dùng mang lại.</p>
                <p>BẠN CÓ MUỐN SỞ HỮU MỘT ĐÔI GIÀY THỂ THAO ĐẸP KHÔNG !?</p>
                <ul>
                    <li>Bạn không hài lòng với đôi giày thể thao hiện tại !!</li>
                    <li>Bạn cần một đôi giày thể thao sneaker đẹp và chất lượng ?</li>
                    <li>Công việc của bạn đòi hỏi phải đi lại nhiều ? Bạn cần giày để chạy bộ êm mượt nhẹ, hay dùng để tập gym luyện tập, hoặc mang đi chơi, đi làm...</li>
                    <li>Đôi giày bạn đang mang làm đau chân và hầm bí ?</li>
                </ul>
                <img src="https://bizweb.dktcdn.net/100/275/458/files/free-ship-tphcm-2.png?v=1529825618211" alt="">
                <p>Hiểu được điều này, với mong muốn mang đến cho các bạn và các fan chạy bộ một trải nghiệm tốt về giày thể thao chạy bộ. Vsneaker chuyên giày thể thao chạy bộ đẹp và tốt theo xu hướng mới hiện nay. Những mẫu giày luôn được cập nhật thường xuyên, và được đáng giá cao khả năng chạy tốt cũng như mang thoải mái được dành riêng cho bạn.</p>
                <p>Với nhiều mẫu mã đẹp phong phú và đa dạng về kiểu dáng và màu sắc, bạn dễ dàng lựa chọn cho bản thân một đôi giày ưng ý. Ngoài dòng giày chạy bộ running atletics mà Vsneaker chuyên về, bạn dễ dàng tìm thấy các dòng giày đẹp khác như dòng streetstyle, lifestyle, classic footwear…để bạn có thể sử dụng khi đi dạo, đi chơi, hay đi làm...</p>
                <img src="https://bizweb.dktcdn.net/100/275/458/files/keeprunning.png?v=1529825639867" alt="">
                <p>Tiêu chí cốt lỗi : ”Mẫu giày nhẹ - mang êm thoải mái – chạy mượt tốt”  của Vsneaker cam kết rằng mỗi đôi giày thể thao chạy bộ tại shop chắc chắn làm bạn hài lòng và ưng ý. Những mẫu giày đều phù hợp những xu hướng thời trang mới hiện nay, chất lượng tốt, và cực nhiều ưu đãi hấp dẫn và chế độ bán hàng tốt dành cho bạn. Vsneaker chuyên bán giày thể thao chạy bộ đẹp và tốt.</p>
            </div>
        </div>
    </div>
</div>
@endsection
