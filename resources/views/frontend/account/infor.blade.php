@extends('Layouts.frontend')
@section('content')
<!--title page-->
<div class="title-page"
     style="background-image: url('{{ asset('imager/title-page1.jpg')}}');background-position: center center;background-size: cover;">
    <div class="container">
        <div class="row">
            <div class=" col-md-6 inner-title-page">
                <h1>Tài khoản</h1>
            </div>
        </div>
    </div>
</div>
<!--end title page-->
<div class="container">
    <div class="row content-checkout">

        <div class="col-md-4 billing-detail" style="margin-top: 20px; ">
            <h2 style="text-transform: uppercase; ">Thông tin người dùng</h2>
            <img width="270px" height="328px" src="{{ asset('Uploads/avatar/'.auth()->user()->avatar) }}">
        </div>
       <form action="{{route('account.update',['account'=>\Illuminate\Support\Facades\Auth::user()->id])}}" id="form-edit" method="post" enctype="multipart/form-data" style="width: 100%; margin-top: 65px;" class="col-md-8">
            {{ csrf_field() }}
            <table style="width: 100%">
                <tr>
                    <td class="td-infor">Tên người dùng</td>
                    <td class="td-infor">
                        <input type="text" class="form-control" id="inputWarning" placeholder="Tên người dùng" value="{{$user->name}}" name="name" required>
                        @if ($errors->has('name'))
                            <span style="color: red">{{ $errors->first('name') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="td-infor">Email</td>
                    <td class="td-infor">
                        <input type="text" class="form-control" id="inputWarning" placeholder="Email" value="{{$user->email}}" name="email" required>
                        @if ($errors->has('email'))
                            <span style="color: red">{{ $errors->first('email') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="td-infor">Mật khâu</td>
                    <td class="td-infor">
                        <input type="password" class="form-control" id="inputWarning" placeholder="Mật khẩu" name="password">
                        @if ($errors->has('password'))
                            <span style="color: red">{{ $errors->first('password') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="td-infor">Xác nhận mật khẩu</td>
                    <td class="td-infor">
                        <input type="password" class="form-control" id="inputWarning" placeholder="Xác nhận mật khẩu" name="confirm_password">
                        @if ($errors->has('confirm_password'))
                            <span style="color: red">{{ $errors->first('confirm_password') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="td-infor">Số điện thoại</td>
                    <td class="td-infor">
                        <input type="text" class="form-control" id="inputWarning" placeholder="Điện thoại" value="{{$user->phone}}" name="phone" required>
                        @if ($errors->has('phone'))
                            <span style="color: red">{{ $errors->first('phone') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="td-infor">Địa chỉ</td>
                    <td class="td-infor">
                        <input type="text" class="form-control" id="inputWarning" placeholder="Địa chỉ" value="{{$user->address}}" name="address" required>
                        @if ($errors->has('address'))
                            <span style="color: red">{{ $errors->first('address') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="td-infor">Ảnh đại diện</td>
                    <td class="td-infor">
                        <input type="file" class="form-control" id="inputWarning" placeholder="Ảnh đại diện" name="avatar" value="" style="margin-bottom: 5px;">
                        @if ($errors->has('avatar'))
                            <span style="color: red">{{ $errors->first('avatar') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="td-infor"></td>
                    <td class="td-infor" style="text-align: right;">
                        <button type="submit" class="btn-update" form="form-edit">
                            Cập nhật thông tin
                        </button>
                        {{--<li><a href="{{ route('changePassword.index')}}">Đổi mật khẩu</a></li>--}}
                    </td>
                </tr>
            </table>
    </div>
</div>
@endsection
