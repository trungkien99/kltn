@extends('Layouts.frontend')
@section('content')
    <!--title detail-->
    <div class="title-page"
         style="background-image: url('{{ asset('imager/shop/banner-pr.png')}}');background-position: center center;background-size: cover;">
        <div class="container">
            <div class="row">
                <div class=" col-md-6 inner-title-page">
                    <h1 style="color:#fff;">Cửa hàng</h1>
                    <p><span>Danh mục sản phẩm</p>
                </div>
            </div>
        </div>
    </div>
    <!--end title detail-->
    <!--product list-->
    <div class="container">
        <div class="prodcut-list">
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                    <div class="row header-show-list">
                        <div class="col-md-6 col-sm-12 col-12">
                            <p>Hiển thị {{ count($products) }} sản phẩm</p>
                        </div>
                        <div class="col-md-6 col-sm-12 col-12">
                            <!-- Chức năng lọc sản phẩm theo giá -->
{{--                            <form id="search" action="{{ route('frontend.category.search')}}" method="post">--}}
{{--                            {{ csrf_field() }}--}}
                            <!-- <select class="float-right" name="sort">
                            <option value="1">Sắp xếp mặc định</option>
                            <option value="2">Sắp xếp mới nhất</option>
                            <option value="3">Sắp xếp theo giá: Thấp đến cao</option>
                            <option value="4">Sắp xếp theo giá: Cao đến thấp</option>
                        </select> -->
                        </div>
                    </div>
                    <div class="row product">
                        @foreach($products as $product)
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="card">
                                    <div class="card-img-top">
                                        @if($product->qty_nhap != 0)
                                            <a href="{{ route('frontend.product.show',['id'=>$product->id]) }}"
                                               class="wp-post-image" style="background-color: #dfdfdf">
                                                <img style="max-height: 255px;border: 1px solid #bdb7b7"
                                                     class="image-cover"
                                                     src="{{ asset('Uploads/products/'.$product->image)}}"
                                                     alt="product">
                                            </a>
                                            @if($product->status == 1)
                                                <p class="onnew">New</p>
                                            @elseif($product->status == 0)
                                                <p class="onsale">Sale</p>
                                            @else

                                            @endif
                                            <div class="icon-product">
                                                <a href="{{ route('frontend.cart.insertSecound',['id'=>$product->id])}}"><span
                                                        class="lnr lnr-lock btn prbtn"
                                                        style="background-color: #ffa6a8; color: white; border-radius: 50%; bottom: 5px;"></span></a>
                                                <a href="{{ route('frontend.product.show',['id'=>$product->id]) }}"><span
                                                        class="lnr lnr-magnifier btn prbtn"
                                                        style="background-color: #ffa6a8; color: white; border-radius: 50%;"></span>
                                                </a>
                                            </div>
                                        @else
                                            <a href="#" class="wp-post-image"
                                               style="background-color: #dfdfdf; opacity: 0.5;">
                                                <img style="max-height: 255px" class="image-cover"
                                                     style="opacity: 0.5 !important;"
                                                     src="{{ asset('Uploads/products/'.$product->image)}}"
                                                     alt="product">
                                            </a>
                                            <p class="onsale">Hết hàng</p>
                                        @endif
                                    </div>
                                    <div class="card-body">
                                        <p class="card-title"><a
                                                href="{{ route('frontend.product.show',['id'=>$product->id]) }}">{{$product->name}}</a>
                                        </p>
                                        <p class="woocommerce-loop-product__title"><a
                                                href="{{ route('frontend.product.show',['id'=>$product->id]) }}">{{ trim(substr( $product->product_name, 0, 35 )) }}
                                                ...</a></p>
                                        @if($product->status != 0)
                                            <span class="price">
                                                <ins>
                                                    <span class="woocommerce-Price-amount amount">
                                                        {{number_format($product->price, 0, '', ',')}} <span
                                                            class="woocommerce-Price-currencySymbol">VND</span>
                                                    </span>
                                                </ins>
                                            </span>
                                        @else
                                            <span style="text-decoration: line-through;">{{ number_format(ceil((-$product->price * 100) / ($product->sale - 100)), 0, '', ',') }}
                                                VND</span>
                                            <span class="price">
                                                <ins>
                                                    <span class="woocommerce-Price-amount amount">
                                                        {{--{{ $product->price}}--}}
                                                        {{ number_format($product->price, 0, '', ',') }}
                                                        <span class="woocommerce-Price-currencySymbol">VND</span>
                                                    </span>
                                                </ins>
                                             </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{ $products->links() }}
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="content-blog-left">
                        <div class="search-blog">
                            <!-- Chức năng tìm kiếm sản phẩm -->
                            <!-- <input type="text" name="search" placeholder="Search" class="float-left">
                            <button type="submit" form="search" class="btn float-right"><span class="lnr lnr-chevron-right"></span></button> -->
                            </form>
                        </div>
                    <!-- <div class="filter-price">
                        <h2>Lọc theo giá sản phẩm</h2>
                        <p></p>
                        <form action="{{ route('frontend.category.filter')}}" method="post">
                            <span class="float-left">Giá:
                                {{ csrf_field() }}
                        <select name="filter" style="width: 100px; margin-left: 15px;">
                            <option value="1">0 - 100000</option>
                            <option value="2">100000 - 500000</option>
                            <option value="3"> > 500000</option>
                        </select>
                    </span>

                    <button class="btn float-right">LỌC</button>
                </form>
            </div> -->
                        <div class="category-blog">
                            <h2>Danh mục sản phẩm</h2>
                            @foreach($categories as $category)
                                <a href="{{ route('frontend.category.show',['id'=>$category->id]) }}">{{$category->name}}</a>
                            @endforeach
                        </div>
                        <!-- Chức năng sản phẩm đặc biệt -->
                    <!-- <div class="popular-item">
                        <h2>Sản phẩm đặc biệt</h2>

                    </div>

                    <div class="lastest-img">
                        <img src="{{ asset('imager/portfolio/Portfolio_Single_Images_item.jpg')}}" alt="">
                    </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end product list-->
@endsection
