@extends('Layouts.backend')
@section('content')
    <div class="wrapper-pro">
        <div class="left-sidebar-pro">
            @include('Layouts.partionsBackend.sidebar')
        </div>
        <!-- Header top area start-->
        <div class="content-inner-all">
        @include('Layouts.partionsBackend.menutop')
        <!-- Header top area end-->
            <!-- Breadcome start-->
            <div class="breadcome-area mg-b-30 small-dn">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                <div class="row">
                                    <div class="col-lg-6">
                                        {{--<div class="breadcome-heading">--}}
                                            {{--<form role="search" class="">--}}
                                                {{--<input type="text" placeholder="Search..." class="form-control">--}}
                                                {{--<a href=""><i class="fa fa-search"></i></a>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="col-lg-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="{{ route('productimg.index') }}">Quản lý ảnh sản phẩm</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Breadcome End-->
            <div class="admin-dashone-data-table-area mg-b-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline8-list shadow-reset">
                                {{--<div class="sparkline8-hd">--}}
                                {{--<div class="main-sparkline8-hd">--}}
                                {{--<h1>Quản lý ảnh sản phẩm</h1>--}}
                                {{--<div class="sparkline8-outline-icon">--}}
                                {{--<span class="sparkline8-collapse-link"><i class="fa fa-chevron-up"></i></span>--}}
                                {{--<span><i class="fa fa-wrench"></i></span>--}}
                                {{--<span class="sparkline8-collapse-close"><i class="fa fa-times"></i></span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="sparkline8-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        @if(Session::has('success'))
                                            <div class="alert alert-success" role="alert">
                                                {{ Session::get('success') }}
                                            </div>
                                        @endif
                                        <div id="toolbar">
                                            <button class="btn btn-white btn-xs" data-toggle="modal"
                                                    data-target="#CreatImg">
                                                <i class="fa fa-plus"></i> Thêm mới
                                            </button>
                                            <div class="modal fade" id="CreatImg" tabindex="-1" role="dialog"
                                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Lưu
                                                                lại</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @foreach($errors ->all() as $error)
                                                                <li style="color: red">{{ $error}}</li>
                                                            @endforeach
                                                            <form role="form" method="post"
                                                                  action="{{ route('productimg.store')}}"
                                                                  enctype="multipart/form-data" class="form-create">
                                                                {{ csrf_field() }}
                                                                <div class="form-group has-warning">
                                                                    <label>Ảnh sản phẩm</label>
                                                                    <input type="file" class="form-control"
                                                                           id="inputWarning" placeholder="Ảnh sản phẩm"
                                                                           name="image_detail">
                                                                    <p>{{ $errors->first('image_detail') }}</p>
                                                                    <label>Mã sản phẩm</label>
                                                                    <select name="product_id" class="form-control">
                                                                        @foreach($products as $prod)
                                                                            <option value="{{$prod->id}}">{{$prod->id}} -- {{$prod->product_name}}</option>
                                                                        @endforeach
                                                                    </select>

                                                                    <p>{{ $errors->first('product_id') }}</p>
                                                                </div>
                                                        </div>
                                                        <div class="modal-footer">

                                                            <button type="submit" class="btn btn-primary">Lưu</button>
                                                            </form>
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Đóng
                                                            </button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="table" data-toggle="table" data-pagination="true"
                                               data-show-columns="true"
                                               data-show-toggle="true"
                                               data-resizable="true" data-page-size="5"
                                               data-page-list="[5, 10, 15, 20, 25, 30, 35]" data-cookie="true"
                                               data-cookie-id-table="saveId"
                                               data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                            <tr>
                                                <th data-field="id">ID</th>
                                                <th data-field="product_id">Mã sản phẩm</th>
                                                <th data-field="product_name">Tên sản phẩm</th>
                                                <th data-field="image_detail">Hình ảnh</th>
                                                <th data-field="action" width="100px">Hành động</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($productimgs as $productimg)
                                                <tr>
                                                    <td>{{ $productimg->id }}</td>
                                                    <td>
                                                        @foreach($products as $prod)
                                                            @if($productimg->product_id === $prod->id)
                                                                {{$prod->id}}
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        @foreach($products as $prod)
                                                            @if($productimg->product_id === $prod->id)
                                                                {{$prod->product_name}}
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td><img width="150px" height="150px"
                                                             src="{{ asset('Uploads/Product_detail/'.$productimg->image_detail)}}">
                                                    </td>
                                                    <td style="width: 125px !important;">
                                                        <div class="btn-group project-list-action">
                                                            {{--<a href="#">--}}
                                                            {{--<button class="btn btn-white btn-xs btn-action"--}}
                                                            {{--data-toggle="modal"--}}
                                                            {{--data-target="{{'#UpdateImg'.$productimg->id}}">--}}
                                                            {{--<i class="fa fa-pencil"></i> Sửa--}}
                                                            {{--</button>--}}
                                                            {{--</a>--}}
                                                        </div>
                                                        <div class="btn-group project-list-action">
                                                            <a href="{{ route('productimg.destroy', [ 'id'=> $productimg->id ]) }}" onclick="return confirm('Bạn chắc chứ?')">
                                                                <button class="btn btn-white btn-xs btn-action"
                                                                        data-toggle="modal"
                                                                        data-target="{{'#UpdateImg'.$productimg->id}}">
                                                                    <i class="fa fa-pencil"></i> Xóa
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Transitions End-->
        </div>
    </div>

@endsection
