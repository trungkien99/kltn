@extends('Layouts.backend')
@section('content')
    <div class="wrapper-pro">
        <div class="left-sidebar-pro">
            @include('Layouts.partionsBackend.sidebar')
        </div>
        <!-- Header top area start-->
        <div class="content-inner-all">
        @include('Layouts.partionsBackend.menutop')
        <!-- Header top area end-->
            <div class="admin-dashone-data-table-area mg-b-15">
                <div class="breadcome-area mg-b-30 small-dn">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="breadcome-heading">
                                                {{--<form role="search" class="">--}}
                                                {{--<input type="text" placeholder="Search..." class="form-control">--}}
                                                {{--<a href=""><i class="fa fa-search"></i></a>--}}
                                                {{--</form>--}}
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <ul class="breadcome-menu">
                                                <li><a href="/admin/review/">Quản lý người dùng</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline8-list shadow-reset">
                                {{--<div class="sparkline8-hd">--}}
                                    {{--<div class="main-sparkline8-hd">--}}
                                        {{--<h1>Quản lý người dùng</h1>--}}
                                        {{--<div class="sparkline8-outline-icon">--}}
                                            {{--<span class="sparkline8-collapse-link"><i class="fa fa-chevron-up"></i></span>--}}
                                            {{--<span><i class="fa fa-wrench"></i></span>--}}
                                            {{--<span class="sparkline8-collapse-close"><i class="fa fa-times"></i></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="sparkline8-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        @if(Session::has('success'))
                                            <div class="alert alert-success" role="alert">
                                                {{ Session::get('success') }}
                                            </div>
                                        @endif
                                        <div id="toolbar">
                                            <a href="{{route('user.add')}}">
                                                <!-- data-target="#CreatUser" -->
                                                <button class="btn btn-white btn-xs" data-toggle="modal" >
                                                    <i class="fa fa-plus"></i> Thêm mới
                                                </button>
                                            </a>
                                        </div>

                                        <table id="table" data-toggle="table" data-pagination="true" data-show-columns="true" data-show-toggle="true" data-resizable="true" data-page-size="5" data-page-list="[5, 10, 15, 20, 25]" data-cookie="true" data-cookie-id-table="saveId" data-click-to-select="true" data-toolbar="#toolbar" >
                                            <thead>
                                            <tr>
                                                {{--<th data-field="name">Tên người dùng</th>--}}
                                                <th data-field="id">Tên</th>
                                                <th data-field="name">Quyền</th>
                                                <th data-field="email">Email</th>
                                                <th data-field="phone">Điện thoại</th>
                                                <th data-field="address">Địa chỉ</th>
                                                <th data-field="avatar">Ảnh đại diện</th>
                                                <th data-field="action" width="100px">Hành động</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($users as $user)
                                                <tr>
                                                    {{--<td>{{$user->id}}</td>--}}
                                                    <td>{{$user->name}}</td>
                                                    <td>
                                                        @if($user->level == 1)
                                                            Admin
                                                        @else
                                                            User
                                                        @endif
                                                    </td>
                                                    <td>{{$user->email}}</td>
                                                    <td>{{$user->phone}}</td>
                                                    <td>{{$user->address}}</td>
                                                    <td>
                                                        @if($user->avatar)
                                                            <img width="150px" height="150px" src="{{ asset('Uploads/avatar/'.$user->avatar) }}">
                                                        @else
                                                            <img width="150px" height="150px" src="https://thumbs.dreamstime.com/b/default-avatar-profile-icon-vector-social-media-user-photo-183042379.jpg">
                                                        @endif
                                                    </td>
                                                    <td style="width: 125px !important;">
                                                        <div class="btn-group project-list-action">
                                                            <a href="{{ route('user.showUpdate', ['id'=>$user->id]) }}">
                                                                <button class="btn btn-white btn-xs btn-action">
                                                                    <i class="fa fa-pencil"></i> Sửa
                                                                </button>
                                                            </a>
                                                        </div>
                                                        <div class="btn-group project-list-action">
                                                            <a href="{{ route('user.destroy', ['id'=>$user->id]) }}" onclick="return confirm('Bạn chắc chứ?')">
                                                                <button class="btn btn-white btn-xs" >
                                                                    <i class="fa fa-pencil"></i> Xóa
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Transitions End-->
        </div>
    </div>

@endsection
