@extends('layouts.backend')
@section('content')
    {{--@if ($errors->any())--}}
        {{--<div class="alert alert-danger alert-dismissible" role="alert">--}}
            {{--<ul>--}}
                {{--@foreach ($errors->all() as $error)--}}
                    {{--<li>{{ $error }}</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
            {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
                {{--<span aria-hidden="true">&times;</span>--}}
                {{--<span class="sr-only">Close</span>--}}
            {{--</button>--}}
        {{--</div>--}}
    {{--@endif--}}
    <div class="wrapper-pro">
        <div class="left-sidebar-pro">
            @include('layouts.partionsBackend.sidebar')
        </div>
        <!-- Header top area start-->
        <div class="content-inner-all">
        @include('layouts.partionsBackend.menutop')
        <!-- Header top area end-->
            <br>
            <!-- Breadcome End-->
            <div class="admin-dashone-data-table-area mg-b-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline8-list shadow-reset">
                                <div class="sparkline8-hd">
                                    <div class="main-sparkline8-hd">
                                        <h1>Chỉnh sửa thông tin người dùng "{{$user->name}}"</h1>
                                        <div class="sparkline8-outline-icon">
                                            <span class="sparkline8-collapse-link"><i
                                                    class="fa fa-chevron-up"></i></span>
                                            <span><i class="fa fa-wrench"></i></span>
                                            <span class="sparkline8-collapse-close"><i class="fa fa-times"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="sparkline8-graph" style="text-align: left; ">
                                    {{--@if ( Session::has('alert') )--}}
                                        {{--<div class="alert alert-danger alert-dismissible" role="alert" style="margin: 10px;">--}}
                                            {{--<strong>{{ Session::get('alert') }}</strong>--}}
                                            {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
                                                {{--<span aria-hidden="true">&times;</span>--}}
                                                {{--<span class="sr-only">Close</span>--}}
                                            {{--</button>--}}
                                        {{--</div>--}}
                                    {{--@endif--}}
                                    <form method="post" action="{{route('user.update',['id'=>$user->id])}}" style="width: 700px; padding: 20px;margin-left:25%" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-4">
                                                @if($user->avatar)
                                                    <img style="max-width: 220px; max-height: 300px !important;" src="{{ asset('Uploads/avatar/'.$user->avatar) }}">
                                                @else
                                                    <img style="max-width: 220px; max-height: 300px !important;" src="https://thumbs.dreamstime.com/b/default-avatar-profile-icon-vector-social-media-user-photo-183042379.jpg">
                                                @endif
                                            </div>
                                            <div class="col-lg-8">
                                                <div style="padding: 10px">
                                                    <span>Tên người dùng</span>
                                                    <div class="input-group mb-3">
                                                        <input type="text" style="width: 500px" class="form-control" value="{{$user->name}}" placeholder="Tên người dùng" name="name" aria-describedby="basic-addon1">
                                                    </div>
                                                    @if ($errors->has('name'))
                                                        <span style="color: red">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                                <div style="padding: 10px">
                                                    <span>Email</span>
                                                    <div class="input-group mb-3">
                                                        <input type="text" style="width: 500px" class="form-control" value="{{$user->email}}" placeholder="Email" name="email" aria-describedby="basic-addon1">
                                                    </div>
                                                    @if ($errors->has('email'))
                                                        <span style="color: red">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>
                                                <div style="padding: 10px">
                                                    <span>Mật khẩu</span>
                                                    <div class="input-group mb-3">
                                                        <input type="password" style="width: 500px" class="form-control" placeholder="Mật khẩu" name="password" aria-describedby="basic-addon1">
                                                    </div>
                                                    @if ($errors->has('password'))
                                                        <span style="color: red">{{ $errors->first('password') }}</span>
                                                    @endif
                                                </div>
                                                <div style="padding: 10px">
                                                    <span>Xác nhận mật khẩu</span>
                                                    <div class="input-group mb-3">
                                                        <input type="password" style="width: 500px" class="form-control" placeholder="Xác nhận mật khẩu" name="confirm_password" aria-describedby="basic-addon1">
                                                    </div>
                                                    @if ($errors->has('confirm_password'))
                                                        <span style="color: red">{{ $errors->first('confirm_password') }}</span>
                                                    @endif
                                                </div>
                                                <div style="padding: 10px">
                                                    <span>Ảnh đại diện</span>
                                                    <div class="input-group mb-3">
                                                        <input type="file" name="avatar">
                                                    </div>
                                                    @if ($errors->has('avatar'))
                                                        <span style="color: red">{{ $errors->first('avatar') }}</span>
                                                    @endif
                                                </div>
                                                <div style="padding: 10px">
                                                    <span>phone</span>
                                                    <div class="input-group mb-3">
                                                        <input type="text" value="{{$user->phone}}" style="width: 500px" class="form-control" placeholder="Phone" name="phone" aria-describedby="basic-addon1">
                                                    </div>
                                                    @if ($errors->has('phone'))
                                                        <span style="color: red">{{ $errors->first('phone') }}</span>
                                                    @endif
                                                </div>
                                                <div style="padding: 10px">
                                                    <span>Address</span>
                                                    <div class="input-group mb-3">
                                                        <input type="text" style="width: 500px" value="{{$user->address}}" class="form-control" placeholder="Address" name="address" aria-describedby="basic-addon1">
                                                    </div>
                                                    @if ($errors->has('address'))
                                                        <span style="color: red">{{ $errors->first('address') }}</span>
                                                    @endif
                                                </div>
                                                @if($user->level ==1)
                                                <div style="padding: 10px">
                                                    <span>Quyền</span>
                                                    <div class="input-group mb-3">
                                                        <select name="level">
                                                            <option value="1" @if($user->level == 1)selected @endif>Quản lý</option>
                                                            <option value="2" @if($user->level == 2)selected @endif>Khách hàng</option>
                                                            <option value="3" @if($user->level == 3)selected @endif>Nhân viên</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                @endif
                                                <div style="padding: 10px">
                                                    <input class="btn btn-primary" type="submit" value="Sửa">
                                                </div>
                                            </div>

                                        </div>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Transitions End-->
        </div>
    </div>
@endsection
