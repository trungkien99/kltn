@extends('Layouts.backend')
@section('content')
    <div class="wrapper-pro">
        <div class="left-sidebar-pro">
            @include('Layouts.partionsBackend.sidebar')
        </div>
        <!-- Header top area start-->
        <div class="content-inner-all">
            @include('Layouts.partionsBackend.menutop')
            <div class="breadcome-area mg-b-30 small-dn">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- 		                            <div class="breadcome-heading">
                                                                                <form role="search" class="">
                                                                                    <input type="text" placeholder="Search..." class="form-control">
                                                                                    <a href=""><i class="fa fa-search"></i></a>
                                                                                </form>
                                                                            </div> -->
                                    </div>
                                    <div class="col-lg-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="{{ route('orderBackend.index') }}">Quản lý đơn đặt hàng</a>
                                                <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Chi tiết đơn hàng</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Content -->
            <div class="admin-dashone-data-table-area mg-b-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline8-list shadow-reset">
                                <div class="sparkline8-hd">
                                    <div class="main-sparkline8-hd" style="margin-top: 10px;">
                                        <p class="detail-title"><b>Tên khách hàng:</b> {{ $order->user_name }}</p>
                                        <p class="detail-title"><b>Email:</b> {{ $order->email }}</p>
                                        <p class="detail-title"><b>Địa chỉ:</b> {{ $order->address }}</p>
                                        <p class="detail-title"><b>Điện thoại:</b> {{ $order->phone }}</p>
                                        <p class="detail-title"><b>Ngày đặt hàng:</b> {{ $order->Date }}</p>
                                        <p class="detail-title"><b>Trạng thái:</b>
                                            @if($order->status == 0)
                                                <span style="color: red">Chờ xử lí</span>
                                            @endif
                                            @if($order->status == 1)
                                                <span style="color: blue">Đang giao hàng</span>
                                            @endif
                                            @if($order->status == 2)
                                                <span style="color: green">Đã giao</span>
                                            @endif
                                            @if($order->status == 3)
                                                <span style="color: green">Đã hủy</span>
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="sparkline8-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        <table id="table" data-toggle="table" data-pagination="true"
                                               data-show-columns="true" data-show-toggle="true" data-resizable="true"
                                               data-page-size="5" data-page-list="[5, 10, 15, 20, 25]"
                                               data-cookie="true" data-cookie-id-table="saveId"
                                               data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                            <tr>
                                                <th data-field="product_name">Tên sản phẩm</th>
                                                <th data-field="product_image">Hình ảnh sản phẩm</th>
                                                <th data-field="product_price">Đơn giá</th>
                                                <th data-field="qty">Số lượng</th>
                                                <th data-field="size">Size</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($details as $detail)
                                                <tr>
                                                    <td>{{$detail->product_name}}</td>
                                                    <td><img src="{{asset('Uploads/products/'.\App\Models\Product::find($detail->product_id)['image'])}}" height="300px" alt=""></td>
                                                    <td>{{number_format($detail->product_price, 0, '', ',')}} VND</td>
                                                    <td>{{$detail->qty}}</td>
                                                    <td>{{$detail->size}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @if($order->status == 0)
                                        <div style="text-align: right; margin-top: 10px">
                                            <form role="form" method="post"
                                                  action="{{ route('orderBackend.update',['id'=>$order->id])}}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="email" value="{{$user->email}}">
                                                <button type="submit" class="btn btn-white btn-xs btn-action">Giao
                                                    hàng
                                                </button>
                                            </form>
                                        </div>
                                    @endif
                                    @if($order->status == 1)
                                        <div style="text-align: right; margin-top: 10px">
                                            <form role="form" method="post"
                                                  action="{{ route('orderBackend.update',['id'=>$order->id])}}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="email" value="{{$user->email}}">
                                                <button type="submit" class="btn btn-white btn-xs btn-action">
                                                    Hoàn tất
                                                </button>
                                            </form>
                                        </div>
                                    @endif
                                    @if($order->status == 0 || $order->status ==1)
                                    <div style="text-align: right; margin-top: 10px">
                                        <form role="form" method="post"
                                              action="{{ route('orderBackend.cancel',['id'=>$order->id])}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="email" value="{{$user->email}}">
                                            <button type="submit" class="btn btn-white btn-xs btn-action">Hủy
                                            </button>
                                        </form>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Content-->
        </div>
    </div>
@endsection
