@extends('layouts.backend')
@section('content')
    {{--@if ($errors->any())--}}
        {{--<div class="alert alert-danger alert-dismissible" role="alert">--}}
            {{--<ul>--}}
                {{--@foreach ($errors->all() as $error)--}}
                    {{--<li>{{ $error }}</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
            {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
                {{--<span aria-hidden="true">&times;</span>--}}
                {{--<span class="sr-only">Close</span>--}}
            {{--</button>--}}
        {{--</div>--}}
    {{--@endif--}}
    <div class="wrapper-pro">
        <div class="left-sidebar-pro">
            @include('layouts.partionsBackend.sidebar')
        </div>
        <!-- Header top area start-->
        <div class="content-inner-all">
        @include('layouts.partionsBackend.menutop')
        <!-- Header top area end-->
            <br>
            <!-- Breadcome End-->
            <div class="admin-dashone-data-table-area mg-b-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline8-list shadow-reset">
                                <div class="sparkline8-hd">
                                    <div class="main-sparkline8-hd">
                                        <h1>Thêm danh mục</h1>
                                        <div class="sparkline8-outline-icon">
                                            <span class="sparkline8-collapse-link"><i
                                                    class="fa fa-chevron-up"></i></span>
                                            <span><i class="fa fa-wrench"></i></span>
                                            <span class="sparkline8-collapse-close"><i class="fa fa-times"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="sparkline8-graph" style="text-align: left; ">
                                    {{--@if ( Session::has('alert') )--}}
                                        {{--<div class="alert alert-danger alert-dismissible" role="alert" style="margin: 10px;">--}}
                                            {{--<strong>{{ Session::get('alert') }}</strong>--}}
                                            {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
                                                {{--<span aria-hidden="true">&times;</span>--}}
                                                {{--<span class="sr-only">Close</span>--}}
                                            {{--</button>--}}
                                        {{--</div>--}}
                                    {{--@endif--}}
                                    <form method="post" action="{{route('category.update', ["id" => $categories->id])}}"
                                          style="width: 700px; padding: 20px;margin-left:25%"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div style="padding: 10px">
                                            <label class="">Tên danh mục</label>
                                            <input id="name" type="text" class="form-control" name="name"
                                                   placeholder="Tên danh mục" value="{{ $categories->name }}">
                                            @if ($errors->has('name'))
                                                <span style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                        <div style="padding: 10px">
                                            <label class="">Mô tả</label>
                                            <textarea id="des" type="text" class="form-control" name="description"
                                                      placeholder="Mô tả">{{ $categories->description }}</textarea>
                                            @if ($errors->has('description'))
                                                <span style="color: red">{{ $errors->first('description') }}</span>
                                            @endif
                                        </div>
                                        <div style="padding: 10px">
                                            <label class="">Từ khóa</label>

                                            <textarea id="key" type="text" class="form-control" name="keywords"
                                                      placeholder="Từ khóa">{{ $categories->keywords }}</textarea>
                                            @if ($errors->has('keywords'))
                                                <span style="color: red">{{ $errors->first('keywords') }}</span>
                                            @endif
                                        </div>
                                        <div style="padding: 10px">
                                            <input type="submit" class="btn btn-primary" value="Sửa">
                                        </div>
                                    </form>
                                    {{--<form role="form" method="POST" action="{{ route('category.store')}}" class="form-create">--}}
		                                                    {{--<div class="form-group has-warning">--}}
		                                                         {{--{{ csrf_field() }}--}}
		                                                        {{--<label class="">Tên danh mục</label>--}}
		                                                        {{--<input id="name" type="text" class="form-control" name="name" placeholder="Tên danh mục" >--}}
		                                                        {{--<p>{{ $errors->first('name') }}</p>--}}

		                                                        {{--<label class="">Mô tả</label>--}}
		                                                        {{--<textarea id="des" type="text" class="form-control" name="description" placeholder="Mô tả"></textarea>--}}
		                                                        {{--<p>{{ $errors->first('description') }}</p>--}}

		                                                        {{--<label class="">Từ khóa</label>--}}
		                                                        {{--<textarea id="key" type="text" class="form-control" name="keywords" placeholder="Từ khóa"></textarea>--}}
		                                                        {{--<p>{{ $errors->first('keywords') }}</p>--}}
		                                                    {{--</div>--}}
		                                                    {{--<div class="modal-footer">--}}
				                                                {{--<button type="submit" class="btn btn-primary">Lưu</button>--}}
				                                            {{--</div>--}}
				                                        {{--</form>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Transitions End-->
        </div>
    </div>
@endsection
