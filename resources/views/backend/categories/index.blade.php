@extends('Layouts.backend')
@section('content')
<div class="wrapper-pro">
    <div class="left-sidebar-pro">
        @include('Layouts.partionsBackend.sidebar')
    </div>
    <!-- Header top area start-->
    <div class="content-inner-all">
        @include('Layouts.partionsBackend.menutop')
        <div class="breadcome-area mg-b-30 small-dn">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="breadcome-heading">
                                        {{--<form role="search" class="">--}}
                                        {{--<input type="text" placeholder="Search..." class="form-control">--}}
                                        {{--<a href=""><i class="fa fa-search"></i></a>--}}
                                        {{--</form>--}}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <ul class="breadcome-menu">
                                        <li><a href="/admin/review/">Quản lý danh mục sản phẩm</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header top area end-->
		<div class="admin-dashone-data-table-area mg-b-15">
		    <div class="container-fluid">
		        <div class="row">
		            <div class="col-lg-12">
		                <div class="sparkline8-list shadow-reset">
		                    {{--<div class="sparkline8-hd">--}}
		                        {{--<div class="main-sparkline8-hd">--}}
		                            {{--<h1>Quản lý danh mục sản phẩm</h1>--}}
		                            {{--<div class="sparkline8-outline-icon">--}}
		                                {{--<span class="sparkline8-collapse-link"><i class="fa fa-chevron-up"></i></span>--}}
		                                {{--<span><i class="fa fa-wrench"></i></span>--}}
		                                {{--<span class="sparkline8-collapse-close"><i class="fa fa-times"></i></span>--}}
		                            {{--</div>--}}
		                        {{--</div>--}}
		                    {{--</div>--}}
		                    <div class="sparkline8-graph">
		                        <div class="datatable-dashv1-list custom-datatable-overright">
                                    @if(Session::has('success'))
                                        <div class="alert alert-success" role="alert">
                                            {{ Session::get('success') }}
                                        </div>
                                    @endif
		                            <div id="toolbar">
		                            	<a href="{{ route('category.show') }}">
			                                <button class="btn btn-white btn-xs" data-toggle="modal" data-target="#CreatCategory">
			                                    <i class="fa fa-plus"></i> Thêm mới
			                                </button>
		                            	</a>
		                            </div>
		                            <table id="table" data-toggle="table" data-pagination="true" data-show-columns="true" data-show-toggle="true" data-resizable="true" data-page-size="5" data-page-list="[5, 10, 15, 20, 25]" data-cookie="true" data-cookie-id-table="saveId" data-click-to-select="true" data-toolbar="#toolbar" >
		                                <thead>
		                                    <tr>
		                                        <th data-field="id">ID</th>
		                                        <th data-field="name">Tên danh mục</th>
		                                        <th data-field="des" >Mô tả</th>
		                                        <th data-field="key" >Từ khóa</th>
		                                        <th data-field="action" width="100px">Hành động</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
		                                    @foreach($categories as $category)
		                                    <tr>
		                                        <td><a style="color: black;" href="{{ route('category.show',['id'=>$category->id])}}">{{$category->id}}</a></td>
		                                        <td><a style="color: black;" href="{{ route('category.show',['id'=>$category->id])}}">{{$category->name}}</a></td>
		                                        <td><a style="color: black;" href="{{ route('category.show',['id'=>$category->id])}}">{{$category->description}}</a></td>
		                                        <td><a style="color: black;" href="{{ route('category.show',['id'=>$category->id])}}">{{$category->keywords}}</a></td>
		                                        <td style="width: 125px !important;">
		                                        	<a href="{{ route('category.showUpdate', ['id'=>$category->id]) }}">
			                                            <div class="btn-group project-list-action">
			                                                <button class="btn btn-white btn-xs btn-action" data-toggle="modal" data-target="{{'#UpdateCategory'.$category->id}}"><i class="fa fa-pencil"></i>Sửa</button>
			                                            </div>
		                                        	</a>
		                                        	<a href="{{ route('category.destroy', ['id'=>$category->id]) }}" onclick="return confirm('Bạn chắc chứ?')">
			                                            <div class="btn-group project-list-action">
			                                                <button class="btn btn-white btn-xs btn-action" data-toggle="modal" data-target="{{'#UpdateCategory'.$category->id}}"><i class="fa fa-pencil"></i>Xóa</button>
			                                            </div>
		                                        	</a>
		                                        </td>
		                                  </tr>
		                                  @endforeach
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
        <!-- Transitions End-->
    </div>
</div>
@endsection
