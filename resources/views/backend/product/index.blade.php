@extends('Layouts.backend')
@section('content')
    <div class="wrapper-pro">
        <div class="left-sidebar-pro">
            @include('Layouts.partionsBackend.sidebar')
        </div>
        <!-- Header top area start-->
        <div class="content-inner-all">
            @include('Layouts.partionsBackend.menutop')
            <div class="breadcome-area mg-b-30 small-dn">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="breadcome-heading">
                                            {{--<form role="search" class="">--}}
                                            {{--<input type="text" placeholder="Search..." class="form-control">--}}
                                            {{--<a href=""><i class="fa fa-search"></i></a>--}}
                                            {{--</form>--}}
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="/admin/review/">Quản lý sản phẩm</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="admin-dashone-data-table-area mg-b-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline8-list shadow-reset">
                                {{--<div class="sparkline8-hd">--}}
                                {{--<div class="main-sparkline8-hd">--}}
                                {{--<h1>Quản lý sản phẩm</h1>--}}
                                {{--<div class="sparkline8-outline-icon">--}}
                                {{--<span class="sparkline8-collapse-link"><i--}}
                                {{--class="fa fa-chevron-up"></i></span>--}}
                                {{--<span><i class="fa fa-wrench"></i></span>--}}
                                {{--<span class="sparkline8-collapse-close"><i class="fa fa-times"></i></span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                <div class="sparkline8-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        @if(Session::has('success'))
                                            <div class="alert alert-success" role="alert">
                                                {{ Session::get('success') }}
                                            </div>
                                        @endif
                                        <div id="toolbar">
                                            <a href="{{route('product.add')}}">
                                                <button class="btn btn-white btn-xs" data-toggle="modal"
                                                        data-target="#CreatProduct">
                                                    <i class="fa fa-plus"></i> Thêm mới
                                                </button>
                                            </a>
                                        </div>
                                        <table id="table" data-toggle="table" data-pagination="true"
                                               data-show-columns="true" data-show-toggle="true"
                                               data-resizable="true" data-page-size="5"
                                               data-page-list="[5, 10, 15, 20, 25]" data-cookie="true"
                                               data-cookie-id-table="saveId"
                                               data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                            <tr>

                                                <th data-field="id">ID</th>
                                                <th data-field="product_name">Tên sản phẩm</th>
                                                <th data-field="sale">Khuyến mãi</th>
                                                <th data-field="price">Đơn giá</th>
                                                <th data-field="image">Hình ảnh</th>
                                                <th data-field="prdescriptions">Mô tả</th>
                                                <th data-field="prkeywords">Từ khóa</th>
                                                <th data-field="category_id">Danh mục</th>
                                                <th data-field="status">Trạng thái</th>
                                                <th data-field="qty_nhap">Số lượng</th>
                                                <th data-field="size">Size</th>
                                                <th data-field="action" width="100px">Hành động</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($products as $product)
                                                <tr>
                                                    <td>{{ $product->id}}</td>
                                                    <td>{{ $product->product_name}}</td>
                                                    <td>
                                                        @if($product->sale)
                                                            {{ $product->sale }} %
                                                        @endif
                                                    </td>
                                                    <td>{{number_format($product->price, 0, '', ',')}} VND</td>
                                                    <td><img width="150px" height="150px"
                                                             src="{{ asset('Uploads/products/'.$product->image) }}"></td>
                                                    <td>{{ $product->prdescriptions}}</td>
                                                    <td>{{ $product->prkeywords}}</td>
                                                    <td>
                                                        @foreach($categories as $item)
                                                            @if($item->id === $product->category_id)
                                                                {{ $item->name }}
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        @if($product->status == 1)
                                                            New
                                                        @endif
                                                        @if($product->status == 0)
                                                            Sale
                                                        @endif
                                                        @if($product->status == 2)
                                                            Normal
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($product->qty_nhap < 0)
                                                            0
                                                        @else
                                                            {{ $product->qty_nhap}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($product->size_id < 0)
                                                            0
                                                        @else
                                                            {{ $product->size_id}}
                                                        @endif
                                                    </td>
                                                    <td style="width: 125px !important;">
                                                        <div class="btn-group project-list-action">
                                                            <a href="{{ route('product.showUpdate', [ 'id' => $product->id ]) }}">
                                                                <button class="btn btn-white btn-xs btn-action"
                                                                        data-toggle="modal"
                                                                        data-target="{{'#UpdateProduct'.$product->id}}">
                                                                    <i class="fa fa-pencil"></i> Sửa
                                                                </button>
                                                            </a>
                                                        </div>
                                                        <div class="btn-group project-list-action">
                                                            <a href="{{ route('product.destroy', [ 'id' => $product->id]) }}"
                                                               onclick="return confirm('Bạn chắc chứ?')">
                                                                <button class="btn btn-white btn-xs btn-action"
                                                                        data-toggle="modal"
                                                                        data-target="{{'#UpdateProduct'.$product->id}}">
                                                                    <i class="fa fa-pencil"></i> Xóa
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Transitions End-->
        </div>
    </div>

@endsection
