@extends('layouts.backend')
@section('content')
    <div class="wrapper-pro">
        <div class="left-sidebar-pro">
            @include('layouts.partionsBackend.sidebar')
        </div>
        <!-- Header top area start-->
        <div class="content-inner-all">
        @include('layouts.partionsBackend.menutop')
        <!-- Header top area end-->
            <br>
            <!-- Breadcome End-->
            <div class="admin-dashone-data-table-area mg-b-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline8-list shadow-reset">
                                <div class="sparkline8-hd">
                                    <div class="main-sparkline8-hd">
                                        <h1>Sửa sản phẩm</h1>
                                        <div class="sparkline8-outline-icon">
                                            <span class="sparkline8-collapse-link"><i
                                                    class="fa fa-chevron-up"></i></span>
                                            <span><i class="fa fa-wrench"></i></span>
                                            <span class="sparkline8-collapse-close"><i class="fa fa-times"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="sparkline8-graph" style="text-align: left; ">
                                    <form method="post" action="{{route('product.update',['id'=>$product->id])}}"
                                          style="width: 700px; padding: 20px;margin-left:25%"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div style="padding: 10px">
                                            <span>Tên sản phẩm</span>
                                            <div class="input-group mb-3">
                                                <input type="text" style="width: 500px" class="form-control"
                                                       value="{{$product->product_name}}" placeholder="Tên sản phẩm"
                                                       name="product_name" aria-describedby="basic-addon1">
                                            </div>
                                            @if ($errors->has('product_name'))
                                                <span style="color: red">{{ $errors->first('product_name') }}</span>
                                            @endif
                                        </div>
                                        <div style="padding: 10px">
                                            <span>Giá</span>
                                            <div class="input-group mb-3">
                                                <input type="number" style="width: 500px" class="form-control"
                                                       value="{{$product->price}}" placeholder="Giá" name="price"
                                                       aria-describedby="basic-addon1">
                                            </div>
                                            @if ($errors->has('price'))
                                                <span style="color: red">{{ $errors->first('price') }}</span>
                                            @endif
                                        </div>
                                        <div style="padding: 10px">
                                            <span>Ảnh sản phẩm</span>
                                            <div class="input-group mb-3">
                                                <td><img width="150px" height="150px"
                                                         src="{{ asset('Uploads/products/'.$product->image) }}">
                                                </td>
                                                <input type="file" name="image">
                                            </div>
                                            @if ($errors->has('image'))
                                                <span style="color: red">{{ $errors->first('image') }}</span>
                                            @endif
                                        </div>
                                        <div style="padding: 10px">
                                            <span>Loại sản phẩm</span>
                                            <div class="input-group mb-3">
                                                <select name="category_id">
                                                    @foreach($categories as $category)
                                                        @if ($product->category_id == $category->id)
                                                            <option value="{{$category->id}}"
                                                                    selected>{{$category->name}}</option>
                                                        @else
                                                            <option
                                                                value="{{$category->id}}">{{$category->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div style="padding: 10px">
                                            <span>Loại sản phẩm</span>
                                            <div class="input-group mb-6">
                                                <select class="form-control select2"  multiple="multiple" style="width: 100%;" id="size-product" onchange="getSize()">
                                                    @for($x =0;$x < count($product['size_id']);$x ++)
                                                        <option value="{{$product['size_id'][$x]}}" selected>{{$product['size_id'][$x]}}</option>
                                                    @endfor
                                                </select>
                                                <input type="hidden" name="size" value="" id="input-size">
                                            </div>
                                        </div>
                                        <div style="padding: 10px">
                                            <span>Miêu tả</span>
                                            <div class="input-group mb-3">
                                                <textarea style="width: 500px;height: 300px; padding: 10px"
                                                          name="prdescriptions"
                                                          placeholder="Miêu tả">{{$product->prdescriptions}}</textarea>
                                            </div>
                                            @if ($errors->has('prdescriptions'))
                                                <span style="color: red">{{ $errors->first('prdescriptions') }}</span>
                                            @endif
                                        </div>
                                        <div style="padding: 10px">
                                            <span>Số lượng</span>
                                            <div class="input-group mb-3">
                                                <input type="number" style="width: 500px" class="form-control"
                                                       value="{{$product->qty_nhap}}" placeholder="Số lượng hàng"
                                                       name="qty_nhap" aria-describedby="basic-addon1">
                                            </div>
                                            @if ($errors->has('qty_nhap'))
                                                <span style="color: red">{{ $errors->first('qty_nhap') }}</span>
                                            @endif
                                        </div>
                                        <div style="padding: 10px">
                                            <span>Từ khóa</span>
                                            <div class="input-group mb-3">
                                                <input type="text" style="width: 500px" class="form-control"
                                                       value="{{$product->prkeywords}}" placeholder="Từ khóa"
                                                       name="prkeywords" aria-describedby="basic-addon1">
                                            </div>
                                            @if ($errors->has('prkeywords'))
                                                <span style="color: red">{{ $errors->first('prkeywords') }}</span>
                                            @endif
                                        </div>
                                        <div style="padding: 10px">
                                            <span>Trạng thái</span>
                                            <div class="input-group mb-3">
                                                <select name="status">
                                                    <option value="0" @if($product->status == 0) selected @endif>Sale
                                                    </option>
                                                    <option value="1" @if($product->status == 1) selected @endif>New
                                                    </option>
                                                    <option value="2" @if($product->status == 2) selected @endif>Normal
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        @if($product->status == 0)
                                            <div style="padding: 10px">
                                                <span>Ưu đãi</span>
                                                <div class="input-group mb-3">
                                                    <input type="number" name="sale" value="{{ $product->sale }}">
                                                </div>
                                            </div>
                                        @endif
                                        <div style="padding: 10px">
                                            <input type="submit" class="btn btn-primary" value="Sửa">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Transitions End-->
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>

    <script>
        let size = <?= $size ?>;
        let data_size= [];
        for(let i =0; i < size.length;i++)
        {
            data_size.push(size[i]['size'])
        }

        $('.select2').select2({
            data: data_size,
            tags: true,
            maximumSelectionLength: 10,
            tokenSeparators: [',', ' '],
            placeholder: "Select or type keywords",
        });

        function getSize() {
            var select = document.getElementById('size-product');
            var selected = [...select.options]
                .filter(option => option.selected)
                .map(option => option.value);
            document.getElementById("input-size").value = selected
            console.log(selected)
        }
    </script>
@endsection
