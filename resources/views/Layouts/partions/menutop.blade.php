<!--header desktop-->
<div class="header relative">
    <div class="container-fluid search-header">
        <form>
            <input type="text" placeholder="Search">
            <span class="close-search">X</span>
        </form>
    </div>
    <div class="container-fluid header-main fixed" style="border-bottom: 1px solid #ddd;">
        <div class="header-desktop ">
            <div class=" header-menu-desktop d-flex justify-content-between ">
                <div>
                    <div class="logo">
                        <a href="/"><img src="{{ asset('imager/home/Logo.png') }}" alt=""></a>
                    </div>
                </div>
                <div>
                    <div class="menu ">
                        <ul>
                            <li><a href="{{ route('frontend.home.index') }}" class="home">Trang chủ</a>
                            </li>
                            <li><a href="{{ route('frontend.about.show') }}" class="about">Giới thiệu</a>
                            </li>
                            <li><a href="{{ url('/productlist') }}" class="productlist">Cửa hàng</a>
                            </li>
                            <li><a href="{{ route('frontend.contact.show') }}" class="contact">Liên hệ</a></li>
                            @if(auth()->user())
                                <li><a href="{{ route('frontend.account.infor',['id'=>\Illuminate\Support\Facades\Auth::user()->id])}}" class="account">Tài khoản</a>
                                @if(auth()->user()->level === 1 || auth()->user()->level == 3)
                                    <li><a href="{{ route('admin')}}">Admin</a></li>
                                @endif
                                <li><a href="{{ route('logout')}}">Đăng xuất</a></li>
                            @else
                                <li><a href="{{ route('login')}}">Đăng nhập</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="header-right">
                        <ul class="list-inline">
                            @if(Session::has('carts'))
                            <li><a href="{{ route('frontend.cart.list') }}" class="cart-index">
                                <img src="{{ asset('imager/home/bag-2.png') }}" alt=""
                                     style="width: 16px;height: 22px;margin-top: -10px;">

                                <div class="number-cart">
                                    {{session('carts')->totalQty}}
                                </div>
                                </a>
                            </li>
                            @else
                            <li><a href="{{ route('frontend.cart.list') }}" class="cart-index">
                                <img src="{{ asset('imager/home/bag-2.png') }}" alt=""
                                     style="width: 16px;height: 22px;margin-top: -10px;">
                                <div class="number-cart">
                                    0
                                </div>
                                </a>
                                {{--<div class="widget_shopping_cart">--}}
                                    {{--<div class="widget_shopping_cart_content">--}}
                                        {{--<p class="woocommerce-mini-cart__buttons buttons">--}}
                                            {{--<a href="/cart" class="button wc-forward au-btn btn-small">XEM GIỎ HÀNG</a>--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end header desktop-->
