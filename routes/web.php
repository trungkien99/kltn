<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['namespace'=>'Frontend'], function(){
	Route::get('/','HomeController@index')->name('frontend.home.index');

    // sản phẩm
	Route::get('/product/{id}','ProductController@show')->name('frontend.product.show');
	Route::get('/productlist','ProductController@index')->name('frontend.category.index');
    Route::post('/productlist/search','CategoryController@search')->name('frontend.category.search');
    Route::post('/productlist/filter','CategoryController@filter')->name('frontend.category.filter');

    // danh mục
	Route::get('productlist/category/{id}','CategoryController@show')->name('frontend.category.show');
//	Route::get('productlist/size/{id}','CategoryController@index')->name('frontend.category.show');

	// Giới thiệu
	Route::get('/about','AboutController@show')->name('frontend.about.show');

	// Liên hệ
	Route::get('/contact','ContactController@show')->name('frontend.contact.show');

    // Giỏ hàng
	Route::get('/cart','CartController@show')->name('frontend.cart.list');
    Route::get('/cart-order','CartController@showOrder')->name('frontend.cart.list.order');

    Route::get('/cart-dang-giao','CartController@cartdg')->name('frontend.cart.danggiao');
	Route::post('/cart/{id}','CartController@insert')->name('frontend.cart.insert');
    Route::get('/cart-secound/{id}','CartController@insertSecound')->name('frontend.cart.insertSecound');
	Route::get('/cart/delete/{id}','CartController@delete')->name('frontend.cart.delete');
	Route::post('/cart/update','CartController@update')->name('frontend.cart.update');

	Route::post('/review','ReviewController@store')->name('review.create');
    Route::get('/acount/{id}','AccountController@index')->name('frontend.account.infor');
    Route::post('/acount-update/{account}','AccountController@update')->name('account.update');
//    Route::resource('/acount-changepassword/{id}','AccountController@index')->name('account.index');
    Route::get('order', 'OrderController@index')->name('order.index');
    Route::post('order','OrderController@store')->name('order.store');
    Route::get('partner-payment-success',function (){
        return redirect()->route('frontend.cart.list.order')->with('success', 'Đặt hàng thành công');
    })->name('order.payment');

});
Route::group(['namespace'=>'Backend', 'prefix' => 'admin', 'middleware' => ['auth','CheckRole']],function(){
    // User
	Route::get('/','DashboardController@index')->name('admin');
//    Route::middleware(['checkRoleAdmin'])
    Route::get('/user-index','UserController@index')->name('user.index')->middleware('checkRoleAdmin');
    Route::get('/user-index-add','UserController@add')->name('user.add')->middleware('checkRoleAdmin');
    Route::post('/user-store','UserController@store')->name('user.store')->middleware('checkRoleAdmin');
    Route::get('/user-index-update/{id}','UserController@showUpdate')->name('user.showUpdate');
    Route::post('/user-update/{id}','UserController@update')->name('user.update');
    Route::get('/user-destroy/{id}','UserController@destroy')->name('user.destroy')->middleware('checkRoleAdmin');

    // Product
    Route::get('/product-index','ProductController@index')->name('product.index');
    Route::get('/product-index-add','ProductController@add')->name('product.add');
    Route::post('/product-store','ProductController@store')->name('product.store');
    Route::get('/product-index-update/{id}','ProductController@showUpdate')->name('product.showUpdate');
    Route::post('/product-update/{id}','ProductController@update')->name('product.update');
    Route::get('/product-destroy/{id}','ProductController@destroy')->name('product.destroy');

    // Category
    Route::get('/category-index','CategoryController@index')->name('category.index');
    Route::get('/category-index-add','CategoryController@show')->name('category.show');
    Route::post('/category-store','CategoryController@store')->name('category.store');
    Route::get('/category-index-update/{id}','CategoryController@showUpdate')->name('category.showUpdate');
    Route::post('/category-update/{id}','CategoryController@update')->name('category.update');
    Route::get('/category-destroy/{id}','CategoryController@destroy')->name('category.destroy');

    // ProdIMG
    Route::get('/productimg-index','ProductImgController@index')->name('productimg.index');
    Route::post('/productimg-store','ProductImgController@store')->name('productimg.store');
    Route::post('/productimg-update/{id}','ProductImgController@update')->name('productimg.update');
    Route::get('/productimg-destroy/{id}','ProductImgController@destroy')->name('productimg.destroy');

    // Sale
    Route::get('/sale-index','SaleProductController@index')->name('sale.index');
    Route::post('/sale-store','SaleProductController@store')->name('sale.store');
    Route::post('/sale-update/{id}','SaleProductController@update')->name('sale.update');
    Route::post('/sale-destroy/{id}','SaleProductController@destroy')->name('sale.destroy');

//    // Review
    Route::get('/review-admin','ReviewController@index')->name('backend.review.index');
//    Route::post('/review-store','ReviewController@store')->name('review.store');
//    Route::post('/review-update/{id}','ReviewController@update')->name('review.update');
    Route::get('/review-destroy-admin/{id}','ReviewController@destroy')->name('backend.review.destroy');

    // Order
    Route::get('/orderBackend','OrderController@show')->name('orderBackend.show');
    Route::get('/orderBackendSuccess','OrderController@orderBackendSuccess')->name('orderBackend.orderBackendSuccess');
    Route::get('/orderBackendCancel','OrderController@orderBackendCancel')->name('orderBackend.orderBackendCancel');
    Route::get('/orderBackend-index','OrderController@index')->name('orderBackend.index');
	Route::get('/order/{id}','OrderDetailController@index')->name('orderdetail');
    Route::post('/order-update/{id}','OrderDetailController@update')->name('orderBackend.update');
    Route::post('/order-cancel/{id}','OrderDetailController@cancel')->name('orderBackend.cancel');


	Route::get('logout','LoginController@logout');
});
Route::group(['namespace' => 'Auth'], function(){
	Route::get('login', [ 'as' => 'login', 'uses' => 'LoginController@getLogin'])->name('login');
	Route::post('login', [ 'as' => 'login', 'uses' => 'LoginController@postLogin']);
	Route::get('logout', [ 'as' => 'logout', 'uses' => 'LogoutController@getLogout']);
    Route::get('/input-email-forgot-password','ForgotPasswordController@index')->name('ForgotPassword');
    Route::post('/change-password','ResetPasswordController@index')->name('ChangePassword');
    Route::post('/change-password-post/{id}','ResetPasswordController@changePW')->name('changePW');
	Route::get('register', 'RegisterController@getRegister')->name('getRegister');
	Route::post('register', 'RegisterController@postRegister')->name('postRegister');
});

Route::group(['middleware' => 'auth', 'namespace'=>'Frontend'], function(){
	Route::resource('/review','ReviewController');
	Route::resource('/changePassword','ChangePasswordController');
});
