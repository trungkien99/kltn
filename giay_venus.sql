-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 09, 2022 lúc 03:33 AM
-- Phiên bản máy phục vụ: 10.4.21-MariaDB
-- Phiên bản PHP: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `giay_venus`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `keywords`, `created_at`, `updated_at`) VALUES
(14, 'Adidas', 'GIày Adidas', 'adidas', '2022-04-22 23:14:09', '2022-04-22 23:14:09'),
(15, 'Anta', 'Giày thể thao nam Anta', 'anta', '2022-04-22 23:15:28', '2022-04-22 23:15:28'),
(16, 'Nike', 'Giày Nike', 'nike', '2022-04-22 23:16:00', '2022-04-22 23:16:00'),
(17, 'MLB', 'Giày MLB', 'mlb', '2022-04-22 23:17:52', '2022-04-22 23:17:52'),
(18, 'Vans', 'Giày Vans', 'vans', '2022-04-22 23:18:17', '2022-04-22 23:18:17'),
(19, 'Puma', 'Giày Puma', 'puma', '2022-04-22 23:18:44', '2022-04-22 23:18:44');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_06_16_132519_create_categories_table', 1),
(4, '2019_06_16_133108_create_products_table', 2),
(5, '2019_06_16_134148_create_product_imgs_table', 2),
(8, '2019_06_16_141001_create_admin_table', 3),
(11, '2014_10_12_000000_create_users_table', 4),
(13, '2019_06_24_095954_create_sale_products_table', 5),
(14, '2019_06_16_134737_create_orders_table', 6),
(17, '2019_06_16_135617_create_order_details_table', 7),
(18, '2019_06_30_152214_create_reviews_table', 8);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `totalMoney` double(50,2) NOT NULL,
  `Date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `user_name`, `totalMoney`, `Date`, `status`, `created_at`, `updated_at`, `address`, `email`, `phone`, `note`) VALUES
(106, 23, 'trung kien', 3891000.00, '2022-05-05', 0, '2022-05-05 10:05:46', '2022-05-05 10:05:46', 'Ha noi', 'hunterhijackers@gmail.com', '0562880248', NULL),
(107, 25, 'Kiên Phạm', 4500000.00, '2022-05-06', 1, '2022-05-06 06:33:16', '2022-05-06 06:34:49', 'Nam Định', 'hunterhijackers@gmail.com', '0562940248', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(50,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Đang đổ dữ liệu cho bảng `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `product_name`, `product_price`, `qty`, `size`, `note`, `created_at`, `updated_at`) VALUES
(76, 106, 43, 'Giày Tennis Adidas Adizero Ubersonic 4 GZ8465', 3891000.00, 1, '41', '', '2022-05-05 10:05:46', '2022-05-05 10:05:46'),
(77, 107, 34, 'Giày Thể Thao Adidas Senseboost Go White G26945', 2250000.00, 2, '40', '', '2022-05-06 06:33:16', '2022-05-06 06:33:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(50,2) NOT NULL,
  `qty_nhap` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `prdescriptions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `prkeywords` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sale` int(11) DEFAULT NULL,
  `size_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `product_name`, `price`, `qty_nhap`, `image`, `category_id`, `prdescriptions`, `prkeywords`, `status`, `created_at`, `updated_at`, `sale`, `size_id`) VALUES
(33, 'Giày Thể Thao Adidas NMD R1 FV8732', 200000.00, 20, '1650699713_7739362_de14a55891ab9038b19018a7cdc4a656.giay-the-thao-adidas-nmd-r1-fv8732.jpg', 14, 'Giày Thể Thao Adidas NMD R1 FV8732', 'NMD', '2', '2022-04-23 00:41:53', '2022-04-25 03:50:47', 1, '40,41,42'),
(34, 'Giày Thể Thao Adidas Senseboost Go White G26945', 2250000.00, 15, '1650701065_1535485_c800f896a0c6433d46181bd3398e30a5.giay-the-thao-adidas-senseboost-go-white-g26945.jpg', 14, 'Giày Thể Thao Adidas Senseboost Go White G26945', 'Senseboost', '2', '2022-04-23 01:04:25', '2022-05-06 06:33:10', NULL, '40,41,42'),
(35, 'Giày Tennis Adidas Coneo Qt B44686', 950000.00, 2, '1650852017_9318455_18d8359e914b75eb16b6dc6e46e756d1.Giày Tennis Adidas Coneo Qt B44686.jpg', 14, 'Giày Tennis Adidas Coneo Qt B44686', 'Coneo', '2', '2022-04-24 19:00:17', '2022-05-05 10:05:32', NULL, '41,42'),
(37, 'Giày Thể Thao Adidas Questar GZ0611', 2350000.00, 20, '1650852139_1018030_59c497fc91e0add640dbaf56e2793356.Giày Thể Thao Adidas Questar GZ0611.jpg', 14, 'Giày Thể Thao Adidas Questar GZ0611', 'Questar', '1', '2022-04-24 19:02:19', '2022-04-25 20:20:40', NULL, '41'),
(38, 'Giày Tennis Adidas Advantage FY8801', 1560000.00, 15, '1650852284_7517641_934a3c5a4d7e136d48842c680f5754bc.giay-tennis-adidas-advantage-fy8801.jpg', 14, 'Giày Tennis Adidas Advantage FY8801', 'Advantage', '1', '2022-04-24 19:04:44', '2022-04-27 01:58:40', NULL, '40,42'),
(40, 'Giày Tennis Adidas Advantage GZ5300', 1800000.00, 20, '1650852339_1980460_695f34b855e563fcf66d73a424e5fe12.giay-tennis-adidas-advantage-gz5300.jpg', 14, 'Giày Tennis Adidas Advantage GZ5300', 'Advantage', '1', '2022-04-24 19:05:39', '2022-04-24 19:05:39', NULL, '40,41'),
(41, 'Giày Tennis Adidas Grand Court F36392', 1550000.00, 10, '1650852446_6351457_6425f591bc7a426897449e9d459ac24a.giay-tennis-adidas-grand-court-f36392.jpg', 14, 'Giày Tennis Adidas Grand Court F36392', 'Grand Court', '1', '2022-04-24 19:07:26', '2022-04-25 20:37:28', NULL, '39,40'),
(42, 'Giày Tennis Adidas Advantage FY6033', 2150000.00, 12, '1650854132_8384058_48de06d505db16be33140a6d1c23b1dd.giay-tennis-adidas-advantage-fy6033.jpg', 14, 'Giày Tennis Adidas Advantage FY6033', 'Advantage FY6033', '2', '2022-04-24 20:35:32', '2022-04-24 21:34:11', NULL, '40,41'),
(43, 'Giày Tennis Adidas Adizero Ubersonic 4 GZ8465', 3891000.00, 19, '1650959110_5936019_60f89236b53a04bf8b00964c440d158a.giay-tennis-adidas-adizero-ubersonic-4-gz8465.jpg', 14, 'Giày Tennis Adidas Adizero Ubersonic 4 GZ8465', 'Adizero Ubersonic', '1', '2022-04-25 09:45:10', '2022-05-05 10:05:39', NULL, '41,42'),
(44, 'demo', 240000.00, 19, '1651828194_656490_a43c8a334fca4b56ca3b987ca744e54e.tải xuống.jpg', 14, 'demo', 'demo', '0', '2022-05-06 09:09:54', '2022-05-06 09:55:36', 20, '37,39');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_imgs`
--

CREATE TABLE `product_imgs` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_detail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Đang đổ dữ liệu cho bảng `product_imgs`
--

INSERT INTO `product_imgs` (`id`, `image_detail`, `product_id`, `created_at`, `updated_at`) VALUES
(76, '1650852446_6351457_6425f591bc7a426897449e9d459ac24a.giay-tennis-adidas-grand-court-f36392.jpg', 41, '2022-04-24 19:07:26', '2022-04-24 19:07:26'),
(77, '1650854132_8384058_48de06d505db16be33140a6d1c23b1dd.giay-tennis-adidas-advantage-fy6033.jpg', 42, '2022-04-24 19:35:32', '2022-04-24 19:35:32'),
(78, '1650947235_4693047_3c50e3faa7fed87f06e2a73052d36301.giay-tennis-adidas-adizero-ubersonic-4-black-cloud-fx1372.jpg', 33, '2022-04-25 21:27:15', '2022-04-25 21:27:15'),
(79, '1650947268_217653_ed87d61cef7fda668ae70be7e0c6cebf.giay-tennis-adidas-advantage-fy6033.jpg', 33, '2022-04-25 21:27:48', '2022-04-25 21:27:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Đang đổ dữ liệu cho bảng `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `product_id`, `review`, `created_at`, `updated_at`) VALUES
(22, 23, 43, 'demo', '2022-05-05 10:07:25', '2022-05-05 10:07:25');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sale_products`
--

CREATE TABLE `sale_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `sale` double(8,2) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `size`
--

CREATE TABLE `size` (
  `id` int(10) UNSIGNED NOT NULL,
  `size` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `size`
--

INSERT INTO `size` (`id`, `size`) VALUES
(1, 34),
(2, 35),
(3, 36),
(4, 37),
(5, 38),
(6, 39),
(7, 40),
(8, 41),
(9, 42),
(10, 43);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `avatar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `address`, `phone`, `level`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Thị Phương Hà', 'phuongha@gmail.com', '$2y$10$gPA0hB/bcH6pmldsrtl1IOd/y.DJMRpRQWMEvrvAGap4EOI9sT.Ym', 'Hà Nam', '0379093127', 1, 'avatar1.jpg', 'rAPHFeXDlQCjenQ6nffqe56hC9EulnyQTDKGzhuKjCIrVI4Cy0hWGEtsvJdA', '2019-06-21 12:26:24', '2019-06-30 02:13:12'),
(2, 'Trần Bá Linh', 'linh@gmail.com', '$2y$10$r5Akh1SAtRM029ViO109PORvZDXDU7csQkD2yO0WHwT1l52rKV9K.', 'Hà Nội', '0379093120', 2, 'ourteam-item2.jpg', 'sDh9x4HXrBCOJzgBH5qeZwcjVgN8Uv4u1WZBVQsYbp0moh7eDG260xJe07dF', '2019-06-21 14:55:28', '2019-07-03 01:37:34'),
(3, 'Phạm Thùy Linh', 'phamlinh@gmail.com', '$2y$10$TvErsszIs/MKIFze.uUp..qZfxeUFnSadDp8vDicl9ZMonnwpcGzq', 'Đại Từ, Thái Nguyên', '0379093126', 2, 'detail9.jpg', '2iV7Lpa1sgCafdEOkbh2wVeYKamoc7kAb0CF6kAQJSVymts7g1uHZO9iUMI7', '2019-07-02 22:23:43', '2019-07-03 00:21:09'),
(11, 'Đặng Quang Nam 11', 'quangnamd99@gmail.com', '123123', 'My Dinh 1 - Nam Tu Liem - Ha Noi, 322/142/9111', '0396374750', 1, 'xet-nghiem.jpg', NULL, '2021-12-02 07:43:14', '2021-12-06 07:15:00'),
(12, 'Đặng Quang Nam 1', 'quangnamd67@gmail.com', '$2y$10$/2MD2NuPAN7rXq/Wt/UCseAqpdRRvAnyBF3V1RnI7NEXoExxGrIkW', 'My Dinh 1 - Nam Tu Liem - Ha Noi, 322/142/91', '0396374749', 1, '1640014104_2391904_5ea7a1aefe3dab3d68d5b1b16d0b95ad.asdasda.jpg', NULL, '2021-12-02 22:46:13', '2021-12-22 08:02:40'),
(21, 'Quang Nam Đặng', 'quangnamd68@gmail.com', '$2y$10$4gi5AcZVu73Jckkw27IoZOpHkODUSslaxC6DkWxaNCJv5PWByA0Y6', 'ngách 38 - ngõ 20 Hồ Tùng Mậu, Mai Dịch, Cầu Giấy', '0396374745', 1, NULL, NULL, '2021-12-20 06:47:24', '2021-12-20 06:47:24'),
(22, 'phạm trung kiên', 'kien140699@gmail.com', '$2y$10$d4nu02tcoabZD7IVdlq.XuT07K3MWjV8PcQI/rTal4mz3pw7AfPMq', 'Ha noi', '0397240871', 1, 'tải xuống.jpg', NULL, '2022-04-22 23:03:38', '2022-05-06 06:20:12'),
(23, 'Kiên Phạm', 'hunterhijackers@gmail.com', '$2y$10$57Mvg4.Bl2RMUnyboOGorub5e2mWfDhFmyxHSIxlHq37jIciOTxpe', 'Nam Định', '0562940248', 3, NULL, NULL, '2022-05-06 06:25:28', '2022-05-06 06:25:28');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `categories_name_unique` (`name`) USING BTREE;

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Chỉ mục cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`) USING BTREE;

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `products_product_name_unique` (`product_name`) USING BTREE,
  ADD UNIQUE KEY `products_image_unique` (`image`) USING BTREE;

--
-- Chỉ mục cho bảng `product_imgs`
--
ALTER TABLE `product_imgs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `product_imgs_image_detail_unique` (`image_detail`) USING BTREE;

--
-- Chỉ mục cho bảng `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `user_id` (`user_id`) USING BTREE;

--
-- Chỉ mục cho bảng `sale_products`
--
ALTER TABLE `sale_products`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Chỉ mục cho bảng `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE,
  ADD UNIQUE KEY `users_password_unique` (`password`) USING BTREE,
  ADD UNIQUE KEY `users_phone_unique` (`phone`) USING BTREE;

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT cho bảng `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT cho bảng `product_imgs`
--
ALTER TABLE `product_imgs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT cho bảng `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT cho bảng `sale_products`
--
ALTER TABLE `sale_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `size`
--
ALTER TABLE `size`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
