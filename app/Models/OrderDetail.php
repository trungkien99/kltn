<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Order;

class OrderDetail extends Model
{
    protected $fillable = [
    	'order_id',
    	'product_id',
    	'product_name',
    	'product_price',
    	'qty',
        'size',
    	'note'
    ];
    public function order(){
    	return $this->belongsTo(\App\Models\Order::class);
    }
    public function product(){
    	return $this->belongsTo(Product::class);
    }
}
