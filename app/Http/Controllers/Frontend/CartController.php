<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function insertSecound(Request $request, $id)
    {
        $product = Product::find($id);
        $carts = $request->session()->get('carts') ?? collect();
        if($carts->count() == 0){
            $product->qty = 1;
            $carts->push($product);
        }
        else{
            if($carts->where('id', $id)->count() == 0){
                $product->qty = 1;
                $carts->push($product);
            }
            else{
                $product = $carts->where('id',$id)->first();

                $qty = $product->qty + 1;
                foreach ($carts as $key => $item) {
                    if($item->id == $id){
                        $carts[$key]->qty = $qty;
                    }
                }
            }
        }
        $request->session()->put('carts',$carts);
        return redirect(route('frontend.cart.list'));
    }

    public function insert(Request $request, $id)
	{
        $request->validate([
            'size' => 'required',
        ], [
            'size.required' => 'Bạn phải chọn size sản phẩm',
        ]);
		$product = Product::find($id); // thấy thông tin sản phẩm
		$carts = $request->session()->get('carts') ?? collect(); // Lấy đơn hàng trong session
        // Nếu chưa có đơn hàng nào
		if($carts->count() == 0){
			$product->qty = $request->qty_add_cart;
            $product->size = $request->size;
			$carts->push($product);
		}
		else{
            // Nếu CHƯA có đơn hàng là sản phẩm có id này
			if($carts->where('id', $id)->where('size',$request->size)->count() == 0){
			    echo "đơn hàng chưa có";
				$product->qty = $request->qty_add_cart;
                $product->size = $request->size;
                $carts->push($product);
			}
			// Nếu ĐÃ có đơn hàng là sản phẩm có id này
			else{
				$product = $carts->where('id',$id)->first();
				foreach ($carts as $key => $item) {
					if($item->id == $id){
						$carts[$key]->qty = $product->qty + $request->qty_add_cart;
					}
				}
			}
		}
        $product_use = Product::where('id',$id)->first();
        $product_use->qty_nhap = $product_use->qty_nhap -  $request->qty_add_cart;
        $product_use->save();
		$request->session()->put('carts',$carts);
		return redirect(route('frontend.cart.list'));
	}
	public function update(Request $request)
	{
	    echo "vao day";
        $id = DB::table('orders')->insertGetId([
            'user_id' => Auth::user()->id,
            'user_name' => Auth::user()->name,
            'totalMoney' => $request->productsTotal,
            'Date'=> '2019-06-29 07:44:30',
            'status' => 0,
        ]);
        $prods = $request->qty;
        foreach ($prods as $id_prod => $qty){
            $product = Product::find($id_prod);
            DB::table('order_details')->insertGetId([
                'order_id' => $id,
                'product_id' => $id_prod,
                'product_name' => $product->product_name,
                'product_price'=> $product->price,
                'qty' => $qty,
            ]);
        }
        $request->session()->forget('carts');
        return back();
	}

	public function delete(Request $request, $id)
	{
		$carts = $request->session()->get('carts');
        $product_use = Product::where('id',$id)->first();
        $product = $carts->where('id',$id)->first();
        foreach ($carts as $key => $item) {
            if($item->id == $id){
                $product_use->qty_nhap = $product_use->qty_nhap +  $product->qty;
                $product_use->save();
            }
        }
		$key = $carts->search(function($item) use($id){
			return $item->id == $id;
		});
		$carts->pull($key);
		$request->session()->put('carts',$carts);
		return redirect(route('frontend.cart.list'));
	}

    public function show(Request $request)
    {
        if ($request->session()->has('carts')) {
            $products = $request->session()->get('carts');
            $total = 0;
            $totalQty = 0;
            $size = [];
            foreach ($products as $key => $item)
            {
                $total += $products[$key]->price * $products[$key]->qty;
                $totalQty += $products[$key]->qty;
                $size = $products[$key]->size;
            }
            $products->total = $total;
            $products->totalQty = $totalQty;
            $products->size = $size;

        }else{
            $products = [];
        }
        return view('frontend.cart.cart')->with([
            'products' => $products
        ]);
    }

    public function getUrlImage($id){
	    $prod = Product::where("id",$id)->get();
        foreach ($prod as $key => $value) {
            $url = $value->image;
        }
	    return $url;
    }

    public function showOrder(Request $request)
    {
    	$arr = [];
        if (auth()->user() != null)
        {
            $order = Order::where("user_id", auth()->user()->id)->get();
            foreach ($order as $key => $value) {
                $status_order = $value->status;
                $order_detail = OrderDetail::where("order_id", $value->id)->get();
                foreach ($order_detail as $key => $value) {
                    $obj = [
                        "id"=> $value->id,
                        "product_id"=> $value->product_id,
                        "product_name"=> $value->product_name,
                        "product_price"=> $value->product_price,
                        'status_order' => $status_order,
                        "img_prod" => $this->getUrlImage($value->product_id),
                        "qty"=> $value->qty,
                        "size" => $value->size
                    ];
                    array_push($arr, $obj);
                }
            }
        }
    	return view('frontend.cart.cartDangGiao')->with([
    		'products' => $arr
    	]);
    }
}
