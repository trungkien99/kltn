<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\UserUpdateRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function index()
    {
        $user = User::find(auth()->user()->id);
        if(!$user) {
            abort(404);
        }
        return view('frontend.account.infor')->with([
            'user' => $user
        ]);
    }

    public function update(Request $request, $id)
    {

        $user = User::find($id);
        if($user->name !== $request->name){
            $request->validate([
                'name' => 'required',
            ], [
                'name.required' => 'Tên người dùng không được để trống.',
            ]);
        }
        if($user->email !== $request->email){
            $request->validate([
                'email' => 'required|unique:users|email',
            ], [
                'email.required' => 'Email không được để trống.',
                'email.unique' => 'Email đã tồn tại',
                'email.email' => 'Yêu cầu email',
            ]);
        }
        if($user->phone !== $request->phone){
            $request->validate([
                'phone' => 'size:10|unique:users',
            ], [
                'phone.unique' => 'Số điện thoại đã tồn tại',
                'phone.size' => 'Số điện thoại phải 10 kí tự',
            ]);
        }
        if ($request->password) {
            if($user->password !== $request->password){
                $request->validate([
                    'password' => 'min:6',
                    'confirm_password' => 'same:password',
                ], [
                    'password.min' => 'Mật khẩu tối thiểu 6 ký tự.',
                    'password.required' => 'Mật khẩu không được để trống.',
                    'confirm_password.same' => 'Mật khẩu xác nhận không trùng khớp.',
                ]);
            }
        }
        if ($request->hasFile('avatar')) {
            if ($user->avatar !== $request->avatar) {
                $request->validate([
                    'avatar' => 'required|image',
                ], [
                    'avatar.image' => 'Yêu cầu định dạng ảnh.',
                    'avatar.required' => 'Ảnh không được để trống.',
                ]);
            }
        }
        if (!$user) {
            abort(404);
        }
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        if ($request->hasFile('avatar')) {
            $fileName = time() . "_" . rand(0,9999999) . "_" . md5(rand(0,9999999)) . "." . $request->avatar->getClientOriginalName();
            $request->avatar->move('Uploads/avatar', $fileName);
            $user->avatar = $fileName;
        };
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->level = $user->level;
        $user->save();
        return back();
    }
}
