<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\User;
use Carbon\Carbon;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Mail\MailOrderConfirm;
use Mail;
class OrderController extends Controller
{
	public function index()
	{
		return view('frontend.order.order');
	}

    public function store(Request $request)
    {
    	$carts = $request->session()->get('carts') ?? collect();
        $request->validate([
            'user_name' => 'required',
            'address' => 'required|max:255',
            'email' => 'required|email',
            'note' => 'max:255',
            'phone' => 'size:10',
        ], [
            'user_name.required' => 'Tên người dùng không được để trống.',
            'address.required' => 'Địa chỉ không được để trống.',
            'address.max' => 'Địa chỉ không quá 255 kí tự',
            'email.required' => 'Email không được để trống',
            'email.email' => 'Yêu cầu email',
            'note.max' => 'Ghi trú không quá 500 kí tự',
            'phone.size' => 'Số điện thoại phải 10 kí tự',
        ]);
        Order::create([
            'user_name' => $request->user_name,
            'totalMoney' => $carts->total,
            'Date'=>Carbon::now('Asia/Ho_Chi_Minh'),
            'user_id' => auth()->user()->id,
            'address' => $request->address,
            'email' => $request->email,
            'phone' => $request->phone,
            'status' => 0,
            'note' => $request->note,
        ]);
        $orderId = Order::where('user_id', auth()->user()->id )->max('id');
        foreach ($carts as $cart) {
            OrderDetail::create([
                'order_id' => $orderId,
                'product_id' => $cart->id,
                'product_name' => $cart->product_name,
                'product_price' => $cart->price,
                'qty' => $cart->qty,
                'size' => $cart->size,
                'note' => isset($request->note) ? $request->note : '',
            ]);

//            $product = Product::find($cart->id);
//            $qtyNew = $product->qty_nhap - $cart->qty;
//
//            $product->qty_nhap = $qtyNew;
//            $product->save();

        }
        $request->session()->forget('carts');
        if ($request->payment == 'nl')
        {
            $price =0;
            $pd_name = [];
            $qty = 0;
            foreach ($carts as $cart) {
                $price += $cart->price;
                $pd_name[] = 'Tên sp: '.$cart->product_name . ' - Size: ' .$cart->size_id . ' - Số lượng: ' . $cart->qty;
                $qty += $cart->qty;
            }
            $merchant_site_code = 48001;
            $return_url = route('order.payment');
            $receiver = "user@yopmail.com";
            $transaction_info = implode(',',$pd_name);
            $order_code = $request->user_name;
            $price = $price;
            $currency = "VND";
            $quantity = $qty;
            $tax = 0;
            $discount =0;
            $fee_cal =0;
            $fee_shipping =0;
            $order_description =$request->note;
            $buyer_info = $request->user_name."*|*".$request->email."*|*".$request->phone."*|*".$request->address;
            $affiliate_code ="";
            $lang = 'vi';
            $secure_pass = 'edef582f8282b93c91d283b0dbb2ffcf';
            $params = array(
                "merchant_site_code"=> $merchant_site_code,
                "return_url" => $return_url,
                "receiver" =>$receiver,
                "transaction_info" => $transaction_info,
                "order_code" => $order_code,
                "price" => $price,
                "currency" => $currency,
                "quantity" => $quantity,
                "tax" => $tax,
                "discount" => $discount,
                "fee_cal" => $fee_cal,
                "fee_shipping" => $fee_shipping,
                "order_description" => $order_description,
                "buyer_info" => $buyer_info,
                "affiliate_code" => $affiliate_code,
                "lang" => $lang,
                "secure_code" => md5($merchant_site_code.' '.$return_url.' '.$receiver.' '.$transaction_info.' '.$order_code.' '.$price.' '.$currency.' '.$quantity.' '.$tax.' '.$discount.' '.$fee_cal.' '.$fee_shipping.' '.$order_description.' '.$buyer_info.' '.$affiliate_code.' '.$secure_pass),
                "cancel_url" => "http://127.0.0.1:8000",
//            "notify_url" => "http://localhost/",
//            "time_limit" => "2022-04-23T16:15:06+07:00",
            );
            $query_string = http_build_query($params);
            $url = 'https://sandbox.nganluong.vn:8088/nl35/checkout.php?'.$query_string;
            return redirect($url);
        }
//        $order = new Order();
//        $order = Order::findOrFail($orderId);
//        Mail::to($request->email)->send(new MailOrderConfirm($order));
        return redirect()->route('frontend.cart.list.order')->with('success', 'Đặt hàng thành công');
    }
}
