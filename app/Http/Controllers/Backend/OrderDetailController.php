<?php

namespace App\Http\Controllers\Backend;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrderDetail;
class OrderDetailController extends Controller
{
    public function index($id)
    {
        $details = OrderDetail::where('order_id',$id)->with('product')->with('order')->get();
        $order = Order::find($id);
        if ($details != false)
        {
            $user = User::find($details[0]->order->user_id);
        }else{
            $user = [];
        }
        return view('backend.orderDetail.index')->with([
            'details' => $details,
            'user'=> $user,
            'order'=>$order
        ]);
    }

    public function update($id)
    {
        $order = Order::find($id);
        if($order->status == 0){
            $order->status = 1;
        }else{
            $order->status = 2;
        }
        $order->save();
        return back();
    }
    public function cancel(Request $request, $id)
    {
        $order = Order::find($id);
        $order_detail = OrderDetail::where('order_id',$id)->get();
        foreach ($order_detail as $detail)
        {
            $product = Product::find($detail->product_id);
            $product->qty_nhap += $detail->qty;
            $product->save();
        }
        $order->status = 3;
        $order->save();
        return back();
    }
}
