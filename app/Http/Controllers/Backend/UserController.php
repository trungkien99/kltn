<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\User\CreateUserRequest;
use App\Http\Requests\Backend\User\UpdateUserRequest;
use App\Models\User;

class UserController extends Controller
{
    // Hiển thị bảng user
    public function index()
    {
        $users = User::paginate(10);
        return view('backend.user.index', ['users' => $users]);
    }

    // Hiển thị trang thêm user
    public function add()
    {
        return view('backend.user.add.add');
    }

    // Xử lí thêm user
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6',
            'confirm_password' => 'same:password',
            'phone' => 'size:10|unique:users',
        ], [
            'name.required' => 'Tên người dùng không được để trống.',
            'email.required' => 'Email không được để trống.',
            'email.unique' => 'Email đã tồn tại',
            'email.email' => 'Yêu cầu email',
            'password.min' => 'Mật khẩu tối thiểu 6 ký tự.',
            'password.required' => 'Mật khẩu không được để trống.',
            'confirm_password.same' => 'Mật khẩu xác nhận không trùng khớp.',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'phone.size' => 'Số điện thoại phải 10 kí tự',
        ]);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'address' => $request->address,
            'phone' => $request->phone,
            'level' => $request->level,
        ]);
        return redirect(route('user.index'))->with('success', 'Thêm thành công!');
    }

    // Hiển thị trang chỉnh sửa user
    public function showUpdate($id)
    {
        $user = User::find($id);
        return view('backend.user.edit')->with([
            'user' => $user
        ]);
    }

    // Xử lí chỉnh sửa user
    public function update(Request $request, $id)
    {

        $user = User::find($id);
        if($user->name !== $request->name){
            $request->validate([
                'name' => 'required',
            ], [
                'name.required' => 'Tên người dùng không được để trống.',
            ]);
        }
        if($user->email !== $request->email){
            $request->validate([
                'email' => 'required|unique:users|email',
            ], [
                'email.required' => 'Email không được để trống.',
                'email.unique' => 'Email đã tồn tại',
                'email.email' => 'Yêu cầu email',
            ]);
        }
        if($user->phone !== $request->phone){
            $request->validate([
                'phone' => 'size:10|unique:users',
            ], [
                'phone.unique' => 'Số điện thoại đã tồn tại',
                'phone.size' => 'Số điện thoại phải 10 kí tự',
            ]);
        }
        if ($request->password) {
            if($user->password !== $request->password){
                $request->validate([
                    'password' => 'min:6',
                    'confirm_password' => 'same:password',
                ], [
                    'password.min' => 'Mật khẩu tối thiểu 6 ký tự.',
                    'password.required' => 'Mật khẩu không được để trống.',
                    'confirm_password.same' => 'Mật khẩu xác nhận không trùng khớp.',
                ]);
            }
        }
        if ($request->hasFile('avatar')) {
            if ($user->avatar !== $request->avatar) {
                $request->validate([
                    'avatar' => 'required|image',
                ], [
                    'avatar.image' => 'Yêu cầu định dạng ảnh.',
                    'avatar.required' => 'Ảnh không được để trống.',
                ]);
            }
        }
        if (!$user) {
            abort(404);
        }
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        if ($request->hasFile('avatar')) {
            $fileName = time() . "_" . rand(0,9999999) . "_" . md5(rand(0,9999999)) . "." . $request->avatar->getClientOriginalName();
            $request->avatar->move('Uploads/avatar', $fileName);
            $user->avatar = $fileName;
        };
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->level = $request->level;
        $user->save();
        return redirect(route('user.index'))->with('success', 'Cập nhật thành công');
    }

    // Xóa user
    public function destroy($id)
    {
        User::destroy($id);
        return redirect(route('user.index'))->with('success', 'Xóa thành công');
    }
}
