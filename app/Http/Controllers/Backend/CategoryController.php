<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Http\Requests\Backend\Category\CreateCategoryRequest;
use App\Http\Requests\Backend\Category\UpdateCategoryRequest;
class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('backend.categories.index')->with([
            'categories' => $categories
        ]);
    }

    public function show()
    {
        return view('backend.categories.add');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required|max:500',
            'keywords' => 'required|max:225',
        ], [
            'name.required' => 'Tên sản phẩm không được để trống.',
            'keywords.required' => 'Từ khóa không được để trống.',
            'keywords.max' => 'Từ khóa nhỏ hơn 225 kí tự',
            'description.max' => 'Miêu tả phải nhỏ hơn 500 kí tự',
            'description.required' => 'Miêu tả không được để trống.',
        ]);
        Category::create($request->all());
        return redirect(route('category.index'))->with('success', 'Thêm thành công!');
    }

    public function showUpdate($id)
    {
        $category = Category::find($id);
        return view('backend.categories.edit')->with([
            'categories' => $category
        ]);
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:categories,name',
            'description' => 'required|max:500',
            'keywords' => 'required|max:225',
        ], [
            'name.required' => 'Tên danh mục không được để trống.',
            'name.unique' => 'Tên danh mục đã tồn tại.',
            'keywords.required' => 'Từ khóa không được để trống.',
            'keywords.max' => 'Từ khóa nhỏ hơn 225 kí tự',
            'description.max' => 'Miêu tả phải nhỏ hơn 500 kí tự',
            'description.required' => 'Miêu tả không được để trống.',
        ]);
        $category = Category::find($id);
        if(!$category) {
            abort(404);
        }
        $category->update($request->all());
        return redirect(route('category.index'))->with('success', 'Cập nhật thành công');
    }

    public function destroy($id)
    {
        Category::destroy($id);
        return redirect(route('category.index'))->with('success', 'Xóa thành công!!!');
    }
}
