<?php

namespace App\Http\Controllers\Backend;

use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Review;
class ReviewController extends Controller
{
    public function index()
    {
        $reviews = Review::all();
        $arr = [];
        foreach ($reviews as $item){
            $prod_name = Product::where('id',$item->product_id)->select(['product_name'])->get();
            $user_name = User::where('id',$item->user_id)->select(['email'])->get();
            $obj = [
                "id" => $item->id,
                "prod_name" => $prod_name[0]->product_name,
                "account" => $user_name[0]->email,
                "review" => $item->review,
            ];
            array_push($arr,$obj);
        }
        return view('backend.review.index')->with([
            'reviews' => $arr
        ]);
    }

    public function destroy($id)
    {
        Review::destroy($id);
        return redirect()->route('backend.review.index')->with('success','Xóa đánh giá thành công');
    }
}
