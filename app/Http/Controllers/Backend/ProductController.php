<?php

namespace App\Http\Controllers\Backend;

use App\Models\ProductImg;
use App\Models\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Product\CreateProductRequest;
use App\Http\Requests\Backend\Product\UpdateProductRequest;
use App\Models\Product;
use App\Models\Category;
use App\Models\Sale;
use Illuminate\Support\Facades\Storage;
class ProductController extends Controller
{

    public function index()
    {
        $products = Product::all();
        $categories = Category::select(['id','name'])->get();
        $sales = Sale::select(['id'])->get();
        return view('backend.product.index')->with([
            'products' => $products,
            'categories' => $categories,
            'sales' => $sales,
        ]);
    }

    public function add()
    {
        $categories = Category::all();
        $size = Size::all();
        return view('backend.product.add')->with([
            'categories'=> $categories,
            'size' => $size
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'product_name' => 'required',
            'price' => 'required',
            'prdescriptions' => 'required|max:500',
            'qty_nhap' => 'required',
            'prkeywords' => 'required|max:225',
            'image' => 'required|image',
            'size' => 'required'
        ], [
            'product_name.required' => 'Tên sản phẩm không được để trống.',
            'price.required' => 'Giá không được để trống.',
            'qty_nhap.required' => 'Số lượng nhập không được để trống.',
            'prkeywords.required' => 'Từ khóa không được để trống.',
            'prkeywords.max' => 'Từ khóa nhỏ hơn 225 kí tự',
            'prdescriptions.max' => 'Miêu tả phải nhỏ hơn 500 kí tự',
            'prdescriptions.required' => 'Miêu tả không được để trống.',
            'image.image' => 'Yêu cầu định dạng ảnh.',
            'image.required' => 'Ảnh không được để trống.',
            'size.required' => 'Không được để trống size.',
        ]);
        $fileName = time() . "_" . rand(0,9999999) . "_" . md5(rand(0,9999999)) . "." . $request->image->getClientOriginalName();
        $request->image->move('Uploads/products/',$fileName);
//        $request->image->move('imager/product',$fileName);
        try {
             $data = Product::create([
                'product_name' => $request->product_name,
                'price' => $request->price - (($request->price * $request->sale) / 100),
                'image' => $fileName,
                'sale' => $request->sale,
                'category_id'=>$request->category_id,
                'prdescriptions' => $request->prdescriptions,
                'prkeywords'=> $request->prkeywords,
                'status' => $request->status,
                'qty_nhap' => $request->qty_nhap,
                'size_id' => $request->size,
            ]);
        }
        catch(Exception $e) {
            unlink('Uploads/'.$fileName);
        }
        return redirect(route('product.index'))->with('success', 'Thêm sản phẩm thành công');

    }

    public function showUpdate($id)
    {
        $categories = Category::all();
        $product = Product::find($id);
        $size = Size::all();
        $product['size_id'] = explode(',',$product['size_id']);
        return view('backend.product.edit')->with([
            'product' => $product,
            'categories' => $categories,
            'size' => $size,
        ]);
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::select(['id'])->get();
        if(!$product){
            abort(404);
        }
        return view('backend.product.update')->with([
            'product' => $product,
            'categories' => $categories
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'product_name' => 'required',
            'price' => 'required',
            'prdescriptions' => 'required|max:500',
            'qty_nhap' => 'required',
            'prkeywords' => 'required|max:225',
            'image' => 'required|image',
            'size_id' => 'required'
        ], [
            'product_name.required' => 'Tên sản phẩm không được để trống.',
            'price.required' => 'Giá không được để trống.',
            'qty_nhap.required' => 'Số lượng nhập không được để trống.',
            'prkeywords.required' => 'Từ khóa không được để trống.',
            'prkeywords.max' => 'Từ khóa nhỏ hơn 225 kí tự',
            'prdescriptions.max' => 'Miêu tả phải nhỏ hơn 500 kí tự',
            'prdescriptions.required' => 'Miêu tả không được để trống.',
            'image.image' => 'Yêu cầu định dạng ảnh.',
            'image.required' => 'Ảnh không được để trống.',
            'size.required' => 'Không được để trống size.',
        ]);
        $product = Product::find($id);
        if(!$product){
            abort(404);
        }
        $product->product_name = $request->product_name;
        $product->price = $request->price;
        $product->sale = $request->sale;
        $product->prdescriptions = $request->prdescriptions;
        $product->prkeywords = $request->prkeywords;
        $product->category_id = $request->category_id;
        $product->status = $request->status;
        $product->qty_nhap = $request->qty_nhap;
        $product->size = $request->size;
        if($request->hasFile('image')){
            $fileName = $request->image->getClientOriginalName();
            $request->image->move('Uploads/products/',$fileName);
            $product->image = $fileName;
        };
        $product->save();
        return redirect(route('product.index'))->with('success','Cập nhật thành công');
    }

    public function destroy($id)
    {
        Product::destroy($id);
        return redirect( route('product.index'))->with('success','Xóa sản phẩm thành công');
    }

}
