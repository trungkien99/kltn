<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    public function index(Request $request)
    {
        $user = User::where('email', $request->email)->get();
        if(count($user) > 0){
            return view('Auth/resetPassword')->with([
                'user' => $user
            ]);
        }else{
            return back()->with([
                'error' => 'Khônng tồn tại người dùng.'
            ]);
        }
    }

    public function changePW(Request $request, $id)
    {
        $request->validate([
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password',
        ], [
            'password.min' => 'Mật khẩu tối thiểu 6 ký tự.',
            'password.required' => 'Mật khẩu không được để trống.',
            'confirm_password.same' => 'Mật khẩu xác nhận không trùng khớp.',
        ]);
        $user = User::find($id);
        $user->password = bcrypt($request->password);
        $user->save();
        return redirect()->route('login')->with('success','Thay đổi mật khẩu thành công');
    }
//    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('guest');
//    }
}
